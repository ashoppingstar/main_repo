//finds mins of year per given months
/*  activity_prices: json object with the query response of the getter, 
        for one year back from now and current_date-sorted
    per_months: per how many months do we want the mins
    today: today
    returns an array of mins from the first price of this year, every per_months
    until today. 

    All time variables: in msec (Javascript)
 */
function min_prices(activity_prices, today, per_months) {
    var mins = [];
    var first_date = activity_prices[0].current_date //the earlier date
    var year = 120//12*30*24*60*60*100
    var index = Math.ceil(year*per_months/12);
    var curr_min = Number.MAX_SAFE_INTEGER;
    var curr_timespace = first_date + index;

    for (var x of activity_prices) {
        if(x.current_date > today) {
            break;
        }
        if (x.current_date > curr_timespace) {
        	if (x.current_date > curr_timespace + index) {
	        	while (x.current_date > curr_timespace + index) { //one timespace has no price
	        		curr_timespace += 2*index;
		            mins.push(curr_min);
		            mins.push(curr_min);
	        	}
	        	curr_min = Number.MAX_SAFE_INTEGER;
	        }
        	else {
	            curr_timespace += index;
	            mins.push(curr_min);
	            curr_min = Number.MAX_SAFE_INTEGER;
	        }
        }
        if (x.price < curr_min) {
            curr_min = x.price;
        }
    }
    mins.push(curr_min);

    return mins;
}


var p = [
{
	"current_date": 30,
	"price": 10
},
{
	"current_date": 100,
	"price": 2
},
{
	"current_date": 110,
	"price": 5
},
{
	"current_date": 120,
	"price": 1
}

]