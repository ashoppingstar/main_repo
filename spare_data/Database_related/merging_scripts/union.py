import datetime
from itertools import product
import sys

start = [(datetime.datetime(2018,1,1,1,0,0),datetime.datetime(2018,1,1,3,0,0))]
new = [(datetime.datetime(2018,1,1,2,0,0),datetime.datetime(2018,1,1,7,0,0))]
#[print((min(min1, min2), max(max1, max2))) for (min1, max1), (min2, max2) in product(start, new) if min1 <= max2 and max1 >= min2]


#datetime.datetime(year,month,day,hour,minute,seconds
for (min1,max1),(min2,max2) in product(start,new):
	if (min1 <= max2 and max1 >= min2):
		print((min(min1,min2),max(max1,max2)))
	else:
		print("no union needed")


#
function merge(ranges) {
    var result = [], last;

    ranges.forEach(function (r) {
        if (!last || r[0] > last[1])
            result.push(last = r);
        else if (r[1] > last[1])
            last[1] = r[1];
    });

    return result;
}
r = [[0800,0935],[0945,1200],[1100,1735],[1600,1700]]
console.log(JSON.stringify(merge(r)));