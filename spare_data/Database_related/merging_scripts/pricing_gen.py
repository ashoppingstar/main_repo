import random
with open ("activities_pricing.txt","a") as file:
	daily = 1.0
	while daily<6:
		monthly=round(random.uniform(3*daily,12*daily),0)
		yearly= round(random.uniform(5*monthly,12*monthly),0)
		file.write(str(round(daily,0)))
		file.write(" ")
		file.write(str(round(monthly,0)))
		file.write(" ")
		file.write(str(round(yearly,0)))
		file.write("\n")
		daily=daily+0.01
file.close()