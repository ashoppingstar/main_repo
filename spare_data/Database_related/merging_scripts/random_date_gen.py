import random

it=1
with open ("commit_date2.txt","a") as file:
	while it<100:
		year=str(round(random.uniform(2019,2020)))
		month=str(round(random.uniform(1,12))).zfill(2)
		day=str(round(random.uniform(1,28))).zfill(2)
		hh=str(round(random.uniform(0,23))).zfill(2)
		mm=str(round(random.uniform(0,59))).zfill(2)
		ss=str(round(random.uniform(0,59))).zfill(2)
		it=it+1
		file.write(year+"-"+month+"-"+day+" "+hh+":"+mm+":"+ss)
		file.write("\n")