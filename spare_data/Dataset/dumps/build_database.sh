mongo softeng --eval 'db.dropDatabase()';

mongoimport --db softeng --collection activities --file activities.json

mongoimport --db softeng --collection sportscenters --file sportscenters.json

mongoimport --db softeng --collection users --file users.json

mongoimport --db softeng --collection prices --file prices.json
