package gr.ntua.ece.softeng18b.client


import gr.ntua.ece.softeng18b.client.model.*
import gr.ntua.ece.softeng18b.client.rest.RestCallFormat
import groovy.json.JsonSlurper
import spock.lang.Shared
import spock.lang.Specification
import spock.lang.Stepwise
import spock.lang.Unroll

@Stepwise
class ProductsFunctionalTest extends Specification {

    @Shared RestAPI api = null
    @Shared def testData = null
    @Shared def productIds = [] //product insertion index to product id
    @Shared def newProductIds = []

    def "initialize api client"(){
        when:
        String host     = System.getProperty("host")
        String port     = System.getProperty("port")
        String protocol = System.getProperty("protocol")
        api = new RestAPI(host, port as Integer, protocol == 'https')

        then:
        noExceptionThrown()
    }

    def "login"() {
        when:
        String username = System.getProperty("username")
        String password = System.getProperty("password")
        api.login(username, password, RestCallFormat.JSON)

        then:
        api.isLoggedIn()
    }

    def "setup test data"() {
        when:
        String pathToJson = System.getProperty("test.json")
        testData = new JsonSlurper().parse(new File(pathToJson), "utf-8")

        then:
        noExceptionThrown()
    }

    @Unroll
    def "post valid product #p"() {
        given:
        Product posted = new Product(
            name: p.name as String,
            description: p.description as String,
            category: p.category as String,
            tags: p.tags as List<String>
        )
        Product returned = api.postProduct(posted, RestCallFormat.JSON)
        productIds.push(returned.getId())

        expect:
        returned.name == posted.name &&
        returned.description == posted.description
        returned.category == posted.category &&
        returned.tags.toSorted() == posted.tags.toSorted() &&
        !returned.withdrawn

        where:
        p << testData.valid_products

    }

    def "post invalid product #p"() {
        given:
        Product posted = new Product(
            name: p.name as String,
            description: p.description as String,
            category: p.category as String,
            tags: p.tags as List<String>
        )
        boolean error = false
        String message
        try {
        	Product returned = api.postProduct(posted, RestCallFormat.JSON)
        	System.out.println(p.name)
    	}
    	catch(Exception e) {
    		error = true
    		message = e.getMessage()
    	}

        expect:
        error == true &&
        message == "Error 400: Bad Request"

        where:
        p << testData.invalid_products

    }

    @Unroll
    def "fetch product query #q"() {
        given:
        Product p = api.getProduct(
            id as String,
            RestCallFormat.JSON
        )

        expect:
        p.id == id &&
        p.name == q.name &&
        p.description == q.description &&
        p.category == q.category &&
        p.tags == q.tags as List<String> &&
        p.withdrawn == false

        where:
        q << testData.valid_products
        id << productIds.reverse()
    }

    @Unroll
    def "put product #p"() {
        given:
        Product product = new Product(
            name: p.name as String,
            description: p.description as String,
            category: p.category as String,
            tags: p.tags as List<String>,
            withdrawn: p.withdrawn as String
        )
        Product returned = api.putProduct(id, product, RestCallFormat.JSON)

        expect:
        returned.name == product.name &&
        returned.description == product.description
        returned.category == product.category &&
        returned.tags.toSorted() == product.tags.toSorted() &&
        returned.withdrawn == p.withdrawn

        where:
        p << testData.put_products
        id << productIds.reverse()

    }

    @Unroll
    def "delete product #id"() {
        given:
        
        api.deleteProduct(id as String, RestCallFormat.JSON)

        expect:
        true

        where:
        id << productIds.reverse()

    }

    @Unroll
    def "fetch product query #q"() {
        given:
        Product p = api.getProduct(
            id as String,
            RestCallFormat.JSON
        )

        expect:
        p.id == id &&
        p.name == q.name &&
        p.description == q.description &&
        p.category == q.category &&
        p.tags == q.tags as List<String> &&
        p.withdrawn == true

        where:
        q << testData.put_products
        id << productIds.reverse()
    }

    def "logout"() {
        when:
        api.logout(RestCallFormat.JSON)

        then:
        !api.isLoggedIn()
    }

    def "login"() {
        when:
        String username = "admin@mail.com"
        String password = "password"
        api.login(username, password, RestCallFormat.JSON)

        then:
        api.isLoggedIn()
    }

    @Unroll
    def "delete product #id as admin"() {
        given:
        
        api.deleteProduct(id as String, RestCallFormat.JSON)

        expect:
        true

        where:
        id << productIds.reverse()

    }

    @Unroll
    def "fetch product query #q"() {
        given:
        boolean error = false
        String message
        try { 
	        Product p = api.getProduct(
	            id as String,
	            RestCallFormat.JSON
	        )
        }
        catch(Exception e) {
    		error = true
    		message = e.getMessage()
    	}

        expect:
        error == true &&
        message == "Error 404: Not Found"

        where:
        q << testData.put_products
        id << productIds.reverse()
    }

    @Unroll
    def "post valid product #p again"() {
        given:
        Product posted = new Product(
            name: p.name as String,
            description: p.description as String,
            category: p.category as String,
            tags: p.tags as List<String>
        )
        Product returned = api.postProduct(posted, RestCallFormat.JSON)
        newProductIds.push(returned.getId())

        expect:
        returned.name == posted.name &&
        returned.description == posted.description
        returned.category == posted.category &&
        returned.tags.toSorted() == posted.tags.toSorted() &&
        !returned.withdrawn

        where:
        p << testData.products

    }


    @Unroll
    def "patch product #id #patch"() {
        given:
        Product returned = api.patchProduct(id as String,
        	patch.field, patch.value as String, RestCallFormat.JSON)

        expect:
        returned.name == patched_product.name as String &&
        returned.description == patched_product.description as String &&
        returned.category == patched_product.category as String &&
        returned.tags.toSorted() == patched_product.tags.toSorted() as List<String> &&
        returned.withdrawn as String == patched_product.withdrawn

        where:
        patch << testData.patch_products
        id << newProductIds.reverse()
        patched_product << testData.patched_products

    }

    def "logout"() {
        when:
        api.logout(RestCallFormat.JSON)

        then:
        !api.isLoggedIn()
    }
}
