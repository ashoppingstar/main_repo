Main repository for SoftEng 2018 NTUA.

The repository implements [this 
one](https://courses.softlab.ntua.gr/softeng/2018b/Project/project-softeng.pdf).

## Ομάδα: AShoppingStar

Μέλη:

Αρβανίτης Χρήστος       03114622  
Βακαλοπούλου Μυρσίνη    03114308  
Δαζέα Ελένη             03114060  
Ζωιτάκη Χαρίκλεια       03114097  
Λιάρτης Γεώργιος-Ιάσων  03114161  
Μπαγάκης Εμμανουήλ     	03114157  


## Οδηγίες για εγκατάσταση του project

1. Clone το repository αυτό

2. Εκτελούμε τα εξής

 ```
cd build_package/
docker-compose build
docker-compose up
```
Με απλό Ctr+C τερματίζουμε με τον σωστό τρόπο τα containers ώστε να διατηρηθούν τα δεδομένα του mongo container.

Αν αντιμετωπίζετε προβλήματα βεβαιωθείτε πως δεν τρέχει παράλληλα κάποιο άλλο mongodb service. 

By default κάνουμε αντιστοίχηση των εξής:

| container    | port |
|--------------|------|
| app_frontend | 8770 |
| app          | 8765 |

Οπότε το frontend είναι διαθέσιμο στην διεύθυνση:

* {https://}localhost:8770/

και το backend στην:

* {https://}localhost:8765/

