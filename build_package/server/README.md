# Notes for the frontend implementation regarding user auth

*Όλα τα σχετικά με το user authentication endpoints περιέχονται στον φάκελο /routes/api/users.js*

1.LOGIN :

```
POST http://localhost:8000/api/users/login
```

Body:
```json
{
  "user": {
    "email": "arvchristos@gmail.com",
    "password": "12345678"
  }
}
```

Response:
```json
{
    "user": {
        "_id": "5be575e1f7e60c02d44801cc",
        "email": "arvchristos@gmail.com",
        "birth_date": "10-12-96",
        "first_name": "Christos",
        "surname": "Arvanitis",
        "username": "arvchristos_lolakias",
        "is_admin": false,
        "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6ImFydmNocmlzdG9zQGdtYWlsLmNvbSIsImlkIjoiNWJlNTc1ZTFmN2U2MGMwMmQ0NDgwMWNjIiwiZXhwIjoxNTQ3MDMwMjQ4LCJpYXQiOjE1NDE4NDYyNDh9.muZxCjcsoar0p6HPB-OV2L3PmcCYjVIC4F5cy1m_EbY"
    }
}
```
Η frontend υλοποίηση πρέπει να παίζει με το token που αποθηκεύεται στο κρυπτογραφημένο token του session: req.session.tokener . Όσο υπάρχει και είναι ορισμένο αυτό ο χρήστης μας είναι συνδεδεμένος. Αλλιώς έχει κάνει logout.

2. SIGN UP 

```
POST http://localhost:8000/api/users/register
```

Body:
```json
{
  "user": {
    "email": "arvchristos@gmail.com",
    "password": "12345678",
    "birth_date": "10-12-96",
    "first_name": "Christos",
    "surname": "Arvanitis",
    "username": "arvchristos_lolakias"
  }
}
```

Response:
```json
{
    "user": {
        "_id": "5be575e1f7e60c02d44801cc",
        "email": "arvchristos@gmail.com",
        "birth_date": "10-12-96",
        "first_name": "Christos",
        "surname": "Arvanitis",
        "username": "arvchristos_lolakias",
        "is_admin": false,
        "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6ImFydmNocmlzdG9zQGdtYWlsLmNvbSIsImlkIjoiNWJlNTc1ZTFmN2U2MGMwMmQ0NDgwMWNjIiwiZXhwIjoxNTQ3MDMwMjQ4LCJpYXQiOjE1NDE4NDYyNDh9.muZxCjcsoar0p6HPB-OV2L3PmcCYjVIC4F5cy1m_EbY"
    }
}
```

3. LOG OUT

```
POST http://localhost:8000/api/users/logout
```

Χρειάζεται authentication token οπότε αυτό πρέπει να υπάρχει στο header του HTTPS request
Για το πώς γίνεται αυτό δείτε [εδώ](https://stackoverflow.com/questions/33505130/how-to-assign-basic-authentication-header-to-xmlhttprequest) . Αν και αφορά javascript, κάπως έτσι είναι σε κάθε γλώσσα που μπορεί να βολεύει.

 Απευθείας γίνεται redirect σε όποια σελίδα ορίσουμε (θα έλεγα την αρχική όταν υπάρχει κάποιο υποτυπώδες frontend).

4. CURRENT INFO 

Λήψη πληροφοριών απο τη βάση για τον συνδεδεμένο χρήστη.

Επειδή απαιτείται ο χρήστης να είναι συνδεδεμένος για να δεί τα δεδομένα του, πάλι όπως και στο προηγούμενο endpoint απαιτείται στο HTTPS header να υπάρχει το token.

```
GET http://localhost:8000/api/users/current_info
```

Response:
```json
{
    "user": {
        "_id": "5be575e1f7e60c02d44801cc",
        "email": "arvchristos@gmail.com",
        "birth_date": "10-12-96",
        "first_name": "Christos",
        "surname": "Arvanitis",
        "username": "arvchristos_lolakias",
        "is_admin": false,
        "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6ImFydmNocmlzdG9zQGdtYWlsLmNvbSIsImlkIjoiNWJlNTc1ZTFmN2U2MGMwMmQ0NDgwMWNjIiwiZXhwIjoxNTQ3MDMwMjQ4LCJpYXQiOjE1NDE4NDYyNDh9.muZxCjcsoar0p6HPB-OV2L3PmcCYjVIC4F5cy1m_EbY"
    }
}
```

5. UPDATE PROFILE

```
PUT http://localhost:8000/api/users/update_profile
```

Body:
```json
//new data 
{
    "email": "arvchristos@newmail.com",
    "password": "12345678new",
    "birth_date": "10-12-96",
    "first_name": "Christos",
    "surname": "Arvanitis",
    "username": "arvchristos_new"
}
```