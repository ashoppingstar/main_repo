var gulp = require('gulp');
var exec = require('child_process').exec;
var uname = 'mail@mail.com';
var pswd = 'password';
var path_to_json1 = 'product-test-data.json';
var path_to_json2 = 'test-data.json';
var robot_command = "./gradlew test --tests gr.ntua.ece.softeng18b.client.ProductsFunctionalTest -Dusername=" + uname + " -Dpassword=" + pswd + " -Dprotocol=https -DIGNORE_SSL_ERRORS=true -Dhost=localhost -Dport=8765 -Dtest.json=" + path_to_json1 + " --rerun-tasks";
//var robot_command = "./gradlew test --tests gr.ntua.ece.softeng18b.client.ObservatoryAPIFunctionalTest -Dusername="+uname+" -Dpassword="+ pswd +" -Dprotocol=https -DIGNORE_SSL_ERRORS=true -Dhost=localhost -Dport=8765 -Dtest.json="+path_to_json+2" --rerun-tasks"
var given_robot_command = "./gradlew test --tests gr.ntua.ece.softeng18b.client.ObservatoryAPIFunctionalTest -Dusername="+uname+" -Dpassword="+ pswd +" -Dprotocol=https -DIGNORE_SSL_ERRORS=true -Dhost=localhost -Dport=8765 -Dtest.json="+path_to_json2+" --rerun-tasks"; 
var clear_command = "bash empty_database.sh"
var fill_command = "bash build_database.sh"
var parent_dir = '../../spare_data/Dataset'
var gradle_dir = "../../"
var cwd = process.cwd()
var client_dir = "../client"
gulp.task('initialize database', function(cb) {
    process.chdir(parent_dir);
    console.log("current pth is -----------------------!!!! ", process.cwd());
    return exec(clear_command, function(err, stdout, stderr) {
        console.log(stdout);
        console.log(stderr);
        cb(err);
    });
});
gulp.task('fill_database', function(cb) {
    process.chdir(parent_dir);
    console.log("current pth is -----------------------!!!! ", process.cwd());
    return exec(fill_command, function(err, stdout, stderr) {
        console.log(stdout);
        console.log(stderr);
        cb(err);
    });
});
gulp.task('runserver', async function(cb) {
    process.chdir(cwd);
    console.log("current pth is -----------------------!!!! ", process.cwd());
    exec("node app.js localhost", function(err, stdout, stderr) {
        console.log(stdout);
        console.log(stderr);
        cb(err);
    });
});
gulp.task('run frontend server', async function(cb) {
    process.chdir(client_dir);
    console.log("current pth is -----------------------!!!! ", process.cwd());
    exec("node app.js", function(err, stdout, stderr) {
        console.log(stdout);
        console.log(stderr);
        cb(err);
    });
});
gulp.task('runrobot', function(cb) {
    process.chdir(gradle_dir);
    console.log("current pth is -----------------------!!!! ", process.cwd());
    exec(robot_command, function(err, stdout, stderr) {
        console.log(stdout);
        console.log(stderr);
        cb(err);
    });
});

gulp.task('run given robot', function(cb) {
    process.chdir(gradle_dir);
    console.log("current pth is -----------------------!!!! ", process.cwd());
    exec(given_robot_command, function(err, stdout, stderr) {
        console.log(stdout);
        console.log(stderr);
        cb(err);
    });
});

gulp.task('install packages', function(cb) {
    process.chdir(cwd);
    console.log("current pth is -----------------------!!!! ", process.cwd());
    exec('sudo npm install', function(err, stdout, stderr) {
        console.log(stdout);
        console.log(stderr);
        console.log(err)
        cb(err);
    });
});
gulp.task('install frontend packages', function(cb) {
    process.chdir(client_dir);
    console.log("current pth is -----------------------!!!! ", process.cwd());
    exec('sudo npm install', function(err, stdout, stderr) {
        console.log(stdout);
        console.log(stderr);
        console.log(err)
        cb(err);
    });
});
gulp.task('kill node', function(cb) {
    exec('sudo pkill -f node', function(err, stdout, stderr) {
        console.log(stdout);
        console.log(stderr);
        console.log(err)
        cb(err);
    });
});
gulp.task('build', gulp.parallel('runserver', gulp.series('initialize database', 'install packages', 'install frontend packages', 'runrobot', 'kill node')));
gulp.task('server', gulp.series('runserver'));
gulp.task('init_empty', gulp.series('initialize database', gulp.parallel('runserver', 'run frontend server')));
gulp.task('init_full', gulp.series('initialize database', 'fill_database', gulp.parallel('runserver', 'run frontend server')));

gulp.task('robot_test', gulp.parallel('runserver', gulp.series('initialize database', 'install packages', 'install frontend packages', 'run given robot', 'kill node')));