const mongoose = require('mongoose');
const Users = mongoose.model('Users'); //for user checking on delete
const Metadatas = mongoose.model('Metadatas'); //Metadata info for complex milestones
const Prices = mongoose.model('Prices');
const all_tags = ['Ομαδικά αθλήματα', 'Ατομικά αθλήματα', 'Πολεμικές τέχνες', 'Χοροί', 'Αθλήματα νερού', 'Δραστηριότητες εσωτερικού χώρου', 'Δραστηριότητες εξωτερικού χώρου', 'Χειμερινά']
const validation = require("./validation.js")
module.exports = {
    teamplayer: function(user_email, activity) {
        check_teamplayer(user_email, activity)
    },
    update: function(user_email, activity, sportscenter) {
        var user_milestones = {}
        user_milestones = check_teamplayer(user_milestones, activity) //teamplayer milestone
        check_amphibious(user_email, user_milestones, activity)
        check_colombus(user_email, user_milestones, activity)
        check_explorer(user_email, user_milestones, sportscenter)
        check_allaround(user_email, user_milestones)
        check_multi_sportscenters(user_email, user_milestones, 4)
        //update for all those that do not require synchronous calls
        if (user_milestones != {}) {
            var query = {
                email: user_email
            }
            var update = {
                $set: user_milestones,
                $inc: {
                    'metadata.commits_number': 1
                }
            };
            var options = {
                new: true
            };
            var callback = (err, user) => {
                if (err) {
                    throw err;
                }
                var inner_callback = (err, user) => {
                    if (err) {
                        throw err;
                    }
                }
                //check new commit number and update accordingly those ranges
                var level = "novice"
                if (user.metadata.commits_number == 4) {
                    level = 'apprentice'
                    var query = {
                        "_id": user["_id"]
                    }
                    var update = {
                        $set: {
                            "achievements.level": level
                        }
                    };
                    var options = {
                        new: true
                    };
                    Users.findOneAndUpdate(query, update, options, inner_callback);
                } else if (user.metadata.commits_number == 7) {
                    level = "athlete_star"
                    var query = {
                        "_id": user["_id"]
                    }
                    var update = {
                        $set: {
                            "achievements.level": level
                        }
                    };
                    var options = {
                        new: true
                    };
                    Users.findOneAndUpdate(query, update, options, inner_callback);
                } else if (user.metadata.commits_number == 10) {
                    level = "ambassador"
                    var query = {
                        "_id": user["_id"]
                    }
                    var update = {
                        $set: {
                            "achievements.level": level
                        }
                    };
                    var options = {
                        new: true
                    };
                    Users.findOneAndUpdate(query, update, options, inner_callback);
                } else {}
            };
            Users.findOneAndUpdate(query, update, options, callback);
        }
    }
};

function check_teamplayer(user_milestones, activity) {
    if (activity.tags.includes('ΟΜΑΔΙΚΑ ΑΘΛΗΜΑΤΑ')) {
        user_milestones["achievements.teamplayer"] = true
    }
    return user_milestones
}

function check_amphibious(user_email, user_milestones, activity) {
    var already_water = function(price) {
        console.log('HERE')
        match_object = {} //initialize object for match pipeline stage
        match_object['$or'] = []
        const tags = ['Πολεμικές τέχνες', 'Δραστηριότητες εξωτερικού χώρου', 'Χειμερινά']
        for (var i = 0; i < tags.length; i++) {
            match_object['$or'].push({
                'product_details.tags': tags[i]
            })
        }
        aggregate_array = [];
        //ACTIVITIES
        var aggregate_activities = {
            $lookup: {
                from: 'activities',
                localField: 'product_id',
                foreignField: '_id',
                as: 'product_details'
            }
        }
        var aggregate_act_unwind = {
            $unwind: '$product_details'
        }
        aggregate_array.push(aggregate_activities)
        aggregate_array.push(aggregate_act_unwind)
        aggregate_array.push({
            '$match': match_object
        })
        aggregate_array.push({
            '$limit': 1
        })
        Prices.aggregate(aggregate_array, function(err, price) {
            console.log('in thereeeeee')
            if (price) {
                user_milestones["achievements.amphibious"] = true
                var query = {
                    email: user_email
                }
                var update = {
                    $set: user_milestones
                };
                var options = {
                    new: true
                };
                var callback = (err, user) => {
                    if (err) {
                        throw err;
                    }
                };
                Users.findOneAndUpdate(query, update, options, callback);
            } else {}
        })
    }
    var already_land = function(price) {
        match_object = {} //initialize object for match pipeline stage
        match_object['$or'] = []
        const tags = ['Αθλήματα νερού']
        for (var i = 0; i < tags.length; i++) {
            match_object['$or'].push({
                'product_details.tags': tags[i]
            })
        }
        aggregate_array = [];
        //ACTIVITIES
        var aggregate_activities = {
            $lookup: {
                from: 'activities',
                localField: 'product_id',
                foreignField: '_id',
                as: 'product_details'
            }
        }
        var aggregate_act_unwind = {
            $unwind: '$product_details'
        }
        aggregate_array.push(aggregate_activities)
        aggregate_array.push(aggregate_act_unwind)
        aggregate_array.push({
            '$match': match_object
        })
        aggregate_array.push({
            '$limit': 1
        })
        Prices.aggregate(aggregate_array, function(err, price) {
            if (price) {
                user_milestones["achievements.amphibious"] = true
                var query = {
                    email: user_email
                }
                var update = {
                    $set: user_milestones
                };
                var options = {
                    new: true
                };
                var callback = (err, user) => {
                    if (err) {
                        throw err;
                    }
                };
                Users.findOneAndUpdate(query, update, options, callback);
            } else {}
        })
    }
    if (activity.tags.includes('Αθλήματα νερού')) {
        Prices.findOne({
            user: user_email
        }).then(already_water)
    } else if (activity.tags.includes('Πολεμικές τέχνες') || activity.tags.includes('Δραστηριότητες εξωτερικού χώρου') || activity.tags.includes('Χειμερινά')) {
        Prices.findOne({
            user: user_email
        }).then(already_land)
    } else {}
}
//if this is the first contribution for an activity
function check_colombus(user_email, user_milestones, activity) {
    var colombus_callback = function(activity) {
        if (activity) {
            //do nothing
            return
        } else {
            //so this is the first entry sir you deserve the colombus badge
            user_milestones["achievements.colombus"] = true
            var query = {
                email: user_email
            }
            var update = {
                $set: user_milestones
            };
            var options = {
                new: true
            };
            var callback = (err, user) => {
                if (err) {
                    throw err;
                }
            };
            Users.findOneAndUpdate(query, update, options, callback);
        }
    }
    // be careful with async calls because there is a minor change that the insertion of price is done before this check
    Prices.findOne({
        product_id: activity['_id']
    }).then(colombus_callback)
}
//if this is the first contribution for a sportscenter
function check_explorer(user_email, user_milestones) {
    Users.update({
        email: user_email
    }, {
        $inc: {
            'metadata.commits_number': 1
        }
    });
}
//update commit number and level
function check_allaround(user_email, user_milestones) {
    var allaraound_callback = (err, results) => {
        if (results.length == all_tags.length) {
            user_milestones["achievements.allaround"] = true
            var query = {
                email: user_email
            }
            var update = {
                $set: user_milestones
            };
            var options = {
                new: true
            };
            var callback = (err, user) => {
                if (err) {
                    throw err;
                }
            };
            Users.findOneAndUpdate(query, update, options, callback);
        } else {
            return
        }
    }
    var aggregate_array = [{
        "$match": {
            "user": user_email
        }
    }, {
        "$lookup": {
            from: 'activities',
            localField: 'activity_id',
            foreignField: '_id',
            as: 'product_details'
        }
    }, {
        "$unwind": '$product_details'
    }, {
        "$unwind": "$product_details.tags"
    }, {
        $group: {
            _id: "$product_details.tags",
        }
    }]
    Prices.aggregate(aggregate_array, allaraound_callback)
}
//update commit number and level
function check_multi_sportscenters(user_email, user_milestones, barrier) {
    var multi_callback = (err, results) => {
        if (results.length >= barrier) {
            user_milestones["achievements.multi_sportscenters"] = true
            var query = {
                email: user_email
            }
            var update = {
                $set: user_milestones
            };
            var options = {
                new: true
            };
            var callback = (err, user) => {
                if (err) {
                    throw err;
                }
            };
            Users.findOneAndUpdate(query, update, options, callback);
        } else {
            return
        }
    }
    var aggregate_array = [{
        "$match": {
            "user": user_email
        }
    }, {
        $group: {
            _id: "$sportscenter_id",
        }
    }]
    Prices.aggregate(aggregate_array, multi_callback)
}