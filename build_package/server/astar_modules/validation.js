module.exports = {
    valid_name: function(name) {
        return /^(([Α-ΩΆΊΉΌΈΏΎα-ωίϊΐόάέύϋΰήώA-Za-z0-9]+(\s[Α-ΩΆΊΉΌΈΏΎα-ωίϊΐόάέύϋΰήώA-Za-z0-9]+){0,3}))$/.test(name)
    },
    valid_category: function(category) {
        return true // Μην πειράζετε ετσι κι αλλιώς θα βάλει τυχαία categories και tags
        //check if given tags are from the specified tags-list
        const all_tags = ['Ομαδικά αθλήματα', 'Ατομικά αθλήματα', 'Πολεμικές τέχνες', 'Χοροί', 'Αθλήματα νερού', 'Δραστηριότητες εσωτερικού χώρου', 'Δραστηριότητες εξωτερικού χώρου', 'Χειμερινά']
        return all_tags.includes(category)
    },
    valid_description: function(description) {
        return description.length < 301
    },
    valid_tags: function(tags) {
        //Και αυτό πρέπει να το κάνουμε γενικα
        
    	if(typeof tags === "string") {
    		return tags != ""
		}
        return Array.isArray(tags) && tags.every(function(i){ return typeof i === "string" }) &&  !tags.some(function(i){ return  i === "" })
    },
    valid_withdrawn: function(withdrawn) {
        return /^(true|false)$/.test(withdrawn)
    },
    valid_rating: function(rating) {
        return /^[1-5]$/.test(rating)
    },
    valid_date: function(date) {
        is_leap_year = function(year) {
            if (year % 4 != 0) return false
            else if (year % 100 != 0) return true
            else if (year % 400 != 0) return false
            else return true
        }
        if (!/^[0-9]{4}-[0-9]{1,2}-[0-9]{1,2}$/.test(date)) {
            return false
        }
        var token = date.split('-')
        var year = parseInt(token[0], 10)
        var month = parseInt(token[1], 10)
        var day = parseInt(token[2], 10)
        //console.log("YEAR: ", year, " MONTH: ", month, " DAY: ", day)
        if (year < 1920 || year > 2019 || month == 0 || month > 12) return false;
        var month_length = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
        // Adjust for leap years
        if (is_leap_year(year)) {
            month_length[1] = 29
        }
        // Check the range of the day
        return day > 0 && day <= month_length[month - 1];
    },
    valid_price: function(price) {
        return (/^[0-9]+(.([0-9]+))?$/.test(price) && price < 1001 && price > 0)
    },
    valid_schedule: function(schedule) {
        valid_day_schedule = function(day) {
            if (!day) //this day isn't given
                return true
            if (day.constructor === Array) {
                for (day_elem of day) {
                    if (day_elem.constructor === Array) {
                        if (day_elem.length != 2) {
                            return false
                        }
                        for (hour of day_elem) {
                            if (!/^(([1][0-9][0-5][0-9])|([2][0-3][0-5][0-9])|([1-9][0-5][0-9])|([1-5][0-9])|([1-9])|[0])$/.test(hour)) { //hour must be 1-4 digits, not starting with 0 (excpet it is 0)
                                return false
                            }
                        }
                        return true
                    } else return false
                }
            } else return false
        }
        var mon = valid_day_schedule(schedule.mon)
        var tus = valid_day_schedule(schedule.tus)
        var wed = valid_day_schedule(schedule.wed)
        var thu = valid_day_schedule(schedule.thu)
        var fri = valid_day_schedule(schedule.fri)
        var sat = valid_day_schedule(schedule.sat)
        var sun = valid_day_schedule(schedule.sun)
        return (mon && tus && wed && thu && fri && sat && sun)
    },
    //sportscenters
    valid_webpage: function(str) {
        //function taken from stack overflow 
        var pattern = new RegExp('^(https?:\/\/)?' + // protocol
            '((([a-z\d]([a-z\d-]*[a-z\d])*)\.)+[a-z]{2,}|' + // domain name
            '((\d{1,3}\.){3}\d{1,3}))' + // OR ip (v4) address
            '(\:\d+)?(\/[-a-z\d%_.~+]*)*' + // port and path
            '(\?[;&a-z\d%_.~+=-]*)?' + // query string
            '(\#[-a-z\d_]*)?$', 'i'); // fragment locater
        if (!pattern.test(str)) {
            alert("Please enter a valid URL.");
            return false;
        } else {
            return true;
        }
    },
    
    valid_address: function(address) {
        return true //μέχρι να γίνουμε πιο ελαστικοί
        return /^[Α-ΩΆΊΉΌΈΏΎα-ωίϊΐόάέύϋΰήώA-Za-z]+(\s[Α-ΩΆΊΉΌΈΏΎα-ωίϊΐόάέύϋΰήώA-Za-z]+)?\s[0-9]{1,3}(\,(\s?[Α-ΩΆΊΉΌΈΏΎα-ωίϊΐόάέύϋΰήώA-Za-z]+){1,2}){0,2}$/.test(address)
    }, ////Νωνάκριδος κατι 23, Πόλη (opt)πολη, Περιοχη
    //users
    valid_password: function (password){
        return password.length > 7 && password.length < 20
    },
    valid_email: function (email){
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
    }
};


