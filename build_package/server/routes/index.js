const express = require('express');
const router = express.Router();

const cors = require('cors');

router.use(cors());
router.all('*', cors());
router.use('/observatory', require('./observatory')); //prefix login with api

module.exports = router;