const mongoose = require('mongoose');
const passport = require('passport');
const router = require('express').Router();
const auth = require('../../auth');
const Users = mongoose.model('Users');
const Blacklist = mongoose.model('Blacklists');
const crypto = require('crypto'); //for update_password
const validation = require("../../../../astar_modules/validation.js")
//Get info about the current user, providing its auth token.
router.get('/current_info', auth.required, (req, res, next) => {
    const {
        payload: {
            id
        }
    } = req;
    //verify every move for an auth.required man
    Blacklist.findOne({
        token: req.headers['x-observatory-auth']
    }).then(function(result) {
        if (result) {
            return res.status(403).json({
                errors: {
                    user_error: 'login is required',
                },
            });
        } else {
            validated_token();
        }
    });
    const validated_token = () => {
        //console.log(req.headers['x-observatory-auth'])
        //check if format is valid (5,6 persons only json)
        if (req.query.format && req.query.format != "json") {
            return res.status(400).json({
                errors: {
                    format: 'BAD REQUEST only json for this API',
                },
            });
        }
        return Users.findById(id).then((user) => {
            if (!user) {
                return res.sendStatus(400);
            }
            return res.json({
                user: user.toJSON()
            });
        });
    }
});
//Get list of all users
router.get('/', auth.required, (req, res, next) => {
    //verify every move for an auth.required man
    Blacklist.findOne({
        token: req.headers['x-observatory-auth']
    }).then(function(result) {
        if (result) {
            return res.status(403).json({
                errors: {
                    user_error: 'login is required',
                },
            });
        } else {
            validated_token();
        }
    });
    const validated_token = () => {
        //console.log(err)
        //check if format is valid (5,6 persons only json)
        if (req.query.format && req.query.format != "json") {
            return res.status(400).json({
                errors: {
                    format: 'BAD REQUEST only json for this API',
                },
            });
        }
        Users.find().lean().exec(function(err, users) {
            return res.end(JSON.stringify(users));
        })
    }
});
//Get info of user with id:
router.get('/:_id', auth.required, (req, res, next) => {
    //verify every move for an auth.required man
    Blacklist.findOne({
        token: req.headers['x-observatory-auth']
    }).then(function(result) {
        if (result) {
            return res.status(403).json({
                errors: {
                    user_error: 'login is required',
                },
            });
        } else {
            validated_token();
        }
    });
    const validated_token = () => {
        //check if format is valid (5,6 persons only json)
        if (req.query.format && req.query.format != "json") {
            return res.status(400).json({
                errors: {
                    format: 'BAD REQUEST only json for this API',
                },
            });
        }
        if (!mongoose.Types.ObjectId.isValid(req.params._id)) {
            return res.status(400).json({
                errors: {
                    id: 'Invalid id format',
                },
            });
        }
        return Users.findById(req.params._id).then((user) => {
            if (!user) {
                return res.sendStatus(400);
            }
            return res.json({
                user: user.toAuthJSON()
            });
        });
    }
});
//Delete user with id = _id 
// THIS IS REALLY BAD BAD BAD BECAUSE WE HAVE THIS ONE FDUP IF ONE IS NOT ADMIN. WE SHOULD IMPLEMENT AN ADMIN ACCOUNT TO DO SUCH THINGIES
router.delete('/:_id', auth.required, (req, res, next) => {
    const {
        payload: {
            id
        }
    } = req;
    //verify every move for an auth.required man
    if (req.headers['admin_pass'] != "activ_radar") {
        return res.status(400).json({
            errors: {
                format: 'BAD REQUEST ',
            },
        });
    }
    //check if format is valid (5,6 persons only json)
    if (req.query.format && req.query.format != "json") {
        return res.status(400).json({
            errors: {
                format: 'BAD REQUEST only json for this API',
            },
        });
    }
    if (!mongoose.Types.ObjectId.isValid(req.params._id)) {
        return res.status(400).json({
            errors: {
                id: 'Invalid id format',
            },
        });
    }
    if (req.params._id == id) {
        return res.status(400).json({
            errors: "You cannot delete yourself please contact administrators for deletion",
        });
    }
    //check if user is admin
    return Users.findById(id).then((user) => {
        if (!user) {
            return res.sendStatus(404);
        }
        if (user.is_admin) {
            var query = {
                _id: req.params._id
            };
            Users.remove(query, (err, user) => {
                if (err) {
                    throw err;
                }
                res.json(user);
            });
        } else {
            return res.status(400).json({
                errors: "Not authorized for such action please be a good boye",
            });
        }
        /*  */
    });
});
//Update user with _id from user end scope
router.patch('/update_profile', auth.required, (req, res, next) => {
    const {
        payload: {
            id
        }
    } = req;
    //verify every move for an auth.required man
    Blacklist.findOne({
        token: req.headers['x-observatory-auth']
    }).then(function(result) {
        if (result) {
            return res.status(403).json({
                errors: {
                    user_error: 'login is required',
                },
            });
        } else {
            validated_token();
        }
    });
    const validated_token = () => {
        //check if format is valid (5,6 persons only json)
        if (req.query.format && req.query.format != "json") {
            return res.status(400).json({
                errors: {
                    format: 'BAD REQUEST only json for this API',
                },
            });
        }
        /*
            if (!mongoose.Types.ObjectId.isValid(req.params._id)) { 
                return res.status(400).json({
                    errors: {
                        id: 'Invalid id format',
                    },
                });
            }
        */
        var new_user_data = req.body;
        if (new_user_data.first_name && !validation.valid_name(new_user_data.first_name)) {
            return res.status(400).json({
                errors: {
                    first_name: 'is required',
                },
            });
        }
        if (new_user_data.surname && !validation.valid_name(new_user_data.surname)) {
            return res.status(400).json({
                errors: {
                    surname: 'is required',
                },
            });
        }
        if (new_user_data.email && !validation.valid_email(new_user_data.email)) {
            return res.status(400).json({
                errors: {
                    email: 'is required',
                },
            });
        }
        if (new_user_data.birth_date && !validation.valid_date(new_user_data.birth_date)) {
            return res.status(400).json({
                errors: {
                    birth_date: 'is required',
                },
            });
        }
        if (new_user_data.password && !validation.valid_password(new_user_data.password)) {
            return res.status(400).json({
                errors: {
                    password: 'is required',
                },
            });
        }
        //check if password should be updated
        if (new_user_data.password) {
            //update this thingie
            new_user_data.salt = crypto.randomBytes(16).toString('hex');
            new_user_data.hash = crypto.pbkdf2Sync(new_user_data.password, new_user_data.salt, 10000, 512, 'sha512').toString('hex');
            delete new_user_data.password;
        }
        var query = {
            _id: id
        }
        var update = {
            $set: new_user_data
        };
        var options = {
            new: true
        };
        var callback = (err, user) => {
            if (err) {
                throw err;
            }
            //res.json(user); //returns the previous user (result of findoneandupdate)
            res.json(user.toJSON()); //should add checks that not defined in schema
            //return new user data handy for the new form redirection after updated profile
        };
        Users.findOneAndUpdate(query, update, options, callback);
    }
});
module.exports = router;
