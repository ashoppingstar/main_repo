const mongoose = require('mongoose');
const passport = require('passport');
const router = require('express').Router();
const auth = require('../../auth');
const Sportscenters = mongoose.model('Sportscenters');
const Users = mongoose.model('Users'); //for user checking on delete
var cookieParser = require('cookie-parser');
const Blacklist = mongoose.model('Blacklists');
const validation = require("../../../../astar_modules/validation.js")

//EXTRA ENDPOINTS
//Get list of sportscenters-names&address
router.get('/names', auth.optional, (req, res, next) => {
    //status default to false, if we want to take the names of the withdrawn ones: fix
    Sportscenters.find({
        withdrawn: false
    }, {
        'name': 1,
        'address': 1
    }).sort({
        'name': 1
    }).lean().exec(function(err, sportscenter_names) {
        //Activities.count({withdrawn:false}, function (err,total_count) {
        return res.json({
            sportscenter_names
        });
        //start: start,
        //count: count, 
        //total: total_count,
        //products: [
        //    activities
        //]
        //});
    })
    //})
});
router.post('/', auth.required, (req, res, next) => {
    //verify every move for an auth.required man
    Blacklist.findOne({
        token: req.headers['x-observatory-auth']
    }).then(function(result) {
        if (result) {
            return res.status(403).json({
                errors: {
                    user_error: 'login is required',
                },
            });
        } else {
            validated_token();
        }
    });
    const validated_token = () => {
        const sportscenter = req.body;
        //check if format is valid (5,6 persons only json)
        if (req.query.format && req.query.format != "json") {
            return res.status(400).json({
                errors: {
                    format: 'BAD REQUEST only json for this API',
                },
            });
        }
        if (!sportscenter.name || !validation.valid_name(sportscenter.name)) {
            return res.status(400).json({
                errors: {
                    name: 'is required',
                },
            });
        }
        /*
                if (!sportscenter.extraData.webpage) {
                    return res.status(422).json({
                        errors: {
                            webpage: 'is required',
                        },
                    });
                }
        */
        if (!sportscenter.address || !validation.valid_address(sportscenter.address)) {
            return res.status(400).json({
                errors: {
                    address: 'is required',
                },
            });
        }
        if (!sportscenter.lng) {
            return res.status(400).json({
                errors: {
                    lng: 'is required',
                },
            });
        }
        if (!sportscenter.lat) {
            return res.status(400).json({
                errors: {
                    lat: 'is required',
                },
            });
        }
        //tag check missing
        const finalSportscenter = new Sportscenters(sportscenter);
        return finalSportscenter.save().then(() => res.json(finalSportscenter.toJSON()));
    }
});
//Get list of all sportscenters
router.get('/', auth.optional, (req, res, next) => {
    /************Pagination******************/
    //check NaN parameter
    if (isNaN(req.query.start) && req.query.start) {
        return res.status(400).json({
            errors: {
                format: 'BAD REQUEST for start, usage ...?start=INT',
            },
        });
    } else start = req.query.start ? parseInt(req.query.start) : 0
    //check count parammeter
    if (isNaN(req.query.count) && req.query.count) { //if start is not a num
        return res.status(400).json({
            errors: {
                format: 'BAD REQUEST for start, usage ...?count=INT',
            },
        });
    } else count = req.query.count ? parseInt(req.query.count) : 20
    //check status parameter
    if (req.query.status && (req.query.status != 'ACTIVE' && req.query.status != 'WITHDRAWN')) { //if start is not a num
        return res.status(400).json({
            errors: {
                format: 'BAD REQUEST for status, usage ...?status=ACTIVE|WITHDRAWN',
            },
        });
    } else {
        status = req.query.status ? req.query.status : 'ACTIVE' //ACTIVE || WITHDRAWN
        if (status == "ACTIVE") {
            status = false
        } else {
            status = true
        }
    }
    //check sort parameter
    if (req.query.sort && !/^(id|name)\|(DESC|ASC)$/.test(req.query.sort)) {
        return res.status(400).json({
            errors: {
                format: 'BAD REQUEST for sort, usage ...?sort=id|ASC.....',
            },
        });
    } else {
        sort = (req.query.sort ? req.query.sort : "id|DESC").split("|")
        sort_by = sort[0]
        sort_order = sort[1]
        if (sort_by == 'id') {
            sort_by = '_id'
        }
        if (sort_order == 'ASC') {
            sort_order = 1
        } else {
            sort_order = -1
        }
    }
    //check if format is valid (5,6 persons only json)
    if (req.query.format && req.query.format != "json") {
        return res.status(400).json({
            errors: {
                format: 'BAD REQUEST only json for this API',
            },
        });
    }
    /************End of Pagination************/
    Sportscenters.find({
        withdrawn: [status]
    }).sort({
        [sort_by]: [sort_order]
    }).skip(start).limit(count).lean().exec(function(err, sportscenters) {
        var sportscenters = sportscenters.map(function (x) {
            x.id = x["_id"]
            return x
        });
        Sportscenters.count({
            withdrawn: false
        }, function(err, total_count) {
            return res.json({
                start: start,
                count: count,
                total: total_count,
                shops: sportscenters
            });
        })
    })
});
//Get info of sportscenter with id:
router.get('/:_id', auth.optional, (req, res, next) => {
    //check if format is valid (5,6 persons only json)
    if (req.query.format && req.query.format != "json") {
        return res.status(400).json({
            errors: {
                format: 'BAD REQUEST only json for this API',
            },
        });
    }
    if (!mongoose.Types.ObjectId.isValid(req.params._id)) {
        return res.status(400).json({
            errors: {
                id: 'Invalid id format',
            },
        });
    }
    return Sportscenters.findById(mongoose.Types.ObjectId(req.params._id)).then((sportscenter) => {
        if (!sportscenter) {
            return res.sendStatus(400);
        }
        return res.json(sportscenter.toJSON());
    });
});
//Delete activity with id = _id
router.delete('/:_id', auth.required, (req, res, next) => {
    //verify every move for an auth.required man
    Blacklist.findOne({
        token: req.headers['x-observatory-auth']
    }).then(function(result) {
        if (result) {
            return res.status(403).json({
                errors: {
                    user_error: 'login is required',
                },
            });
        } else {
            validated_token();
        }
    });
    const validated_token = () => {
        //check if format is valid (5,6 persons only json)
        if (req.query.format && req.query.format != "json") {
            return res.status(400).json({
                errors: {
                    format: 'BAD REQUEST only json for this API',
                },
            });
        }
        if (!mongoose.Types.ObjectId.isValid(req.params._id)) {
            return res.status(400).json({
                errors: {
                    id: 'Invalid id format',
                },
            });
        }
        //get user id
        const {
            payload: {
                id
            }
        } = req;
        return Users.findById(id).then((user) => {
            if (!user) {
                return res.sendStatus(404);
            }
            if (user.is_admin) {
                var query = {
                    _id: req.params._id
                };
                Sportscenters.remove(query, (err, sportscenter) => {
                    if (err) {
                        throw err;
                    }
                    res.json({
                        message: "OK"
                    });
                });
            } else {
                var query = {
                    _id: req.params._id
                }
                var update = {
                    $set: {
                        "withdrawn": true
                    }
                };
                var options = {};
                var callback = (err, sportscenter) => {
                    if (err) {
                        throw err;
                    }
                    //res.json(user); //returns the previous user (result of findoneandupdate)
                    res.json({
                        message: "OK"
                    }); //should add checks that not defined in schema
                    //return new user data handy for the new form redirection after updated profile
                };
                Sportscenters.update(query, update, callback);
            }
        });
    }
});
//Update user with _id from user end scope
router.put('/:_id', auth.required, (req, res, next) => {
    //verify every move for an auth.required man
    Blacklist.findOne({
        token: req.headers['x-observatory-auth']
    }).then(function(result) {
        if (result) {
            return res.status(403).json({
                errors: {
                    user_error: 'login is required',
                },
            });
        } else {
            validated_token();
        }
    });
    const validated_token = () => {
        //check if format is valid (5,6 persons only json)
        if (req.query.format && req.query.format != "json") {
            return res.status(400).json({
                errors: {
                    format: 'BAD REQUEST only json for this API',
                },
            });
        }
        if (!mongoose.Types.ObjectId.isValid(req.params._id)) {
            return res.status(400).json({
                errors: {
                    id: 'Invalid id format',
                },
            });
        }
        var id = req.params._id;
        var new_sport_data = req.body;
        //check if all parameters exist 
        var missing_field = (!new_sport_data.name || !new_sport_data.tags || new_sport_data.withdrawn == null || !new_sport_data.address || new_sport_data.lng == null || new_sport_data.lat == null || !new_sport_data.extraData)
        if (missing_field) {
            return res.status(400).json({
                errors: {
                    update: 'all fields are required for full update. Else use patch endpoint',
                },
            });
        }
        if (!validation.valid_name(new_sport_data.name)) {
            return res.status(400).json({
                errors: {
                    name: 'is required',
                },
            });
        }
        if (!validation.valid_tags(new_sport_data.tags)) {
            return res.status(400).json({
                errors: {
                    tags: 'undefined tag',
                },
            });
        }
        if (!validation.valid_withdrawn(new_sport_data.withdrawn)) {
            return res.status(400).json({
                errors: {
                    withdrawn: 'Should be true or false',
                },
            });
        }
        if (!validation.valid_address(new_sport_data.address)) {
            return res.status(400).json({
                errors: {
                    address: 'Should be true or false',
                },
            });
        }
        if (new_sport_data.extraData.webpage) {
            if (!validation.valid_webpage(price.extraData.webpage)) {
                return res.status(400).json({
                    errors: {
                        extraData_webpage: 'Should be in correct format:',
                    },
                });
            }
        }
        var update = {
            name: new_sport_data.name,
            tags: new_sport_data.tags,
            withdrawn: new_sport_data.withdrawn,
            address: new_sport_data.address,
            lng: new_sport_data.lng,
            lat: new_sport_data.lat,
            extraData: new_sport_data.extraData,
        }
        var query = {
            _id: id
        };
        var options = {
            new: true
        };
        var callback = (err, sportscenter) => {
            if (err) {
                throw err;
            }
            //res.json(user); //returns the previous user (result of findoneandupdate)
            res.json(sportscenter.toJSON()); //should add checks that not defined in schema
            //return new user data handy for the new form redirection after updated profile
        };
        Sportscenters.findOneAndUpdate(query, update, options, callback);
    }
});
//Update user with _id from user end scope
router.patch('/:_id', auth.required, (req, res, next) => {
    //verify every move for an auth.required man
    Blacklist.findOne({
        token: req.headers['x-observatory-auth']
    }).then(function(result) {
        if (result) {
            return res.status(403).json({
                errors: {
                    user_error: 'login is required',
                },
            });
        } else {
            validated_token();
        }
    });
    const validated_token = () => {
        //check if format is valid (5,6 persons only json)
        if (req.query.format && req.query.format != "json") {
            return res.status(400).json({
                errors: {
                    format: 'BAD REQUEST only json for this API',
                },
            });
        }
        if (!mongoose.Types.ObjectId.isValid(req.params._id)) {
            return res.status(400).json({
                errors: {
                    id: 'Invalid id format',
                },
            });
        }
        var id = req.params._id;
        var new_sport_data = req.body;
        if (new_sport_data.name) {
            if (!validation.valid_name(new_sport_data.name)) {
                return res.status(400).json({
                    errors: {
                        name: 'is required',
                    },
                });
            }
        }
        if (new_sport_data.tags) {
            if (!validation.valid_tags(new_sport_data.tags)) {
                return res.status(400).json({
                    errors: {
                        tags: 'undefined tag',
                    },
                });
            }
        }
        if (new_sport_data.withdrawn) {
            if (!validation.valid_withdrawn(new_sport_data.withdrawn)) {
                return res.status(400).json({
                    errors: {
                        withdrawn: 'Should be true or false',
                    },
                });
            }
        }
        if (new_sport_data.address) {
            if (!validation.valid_address(new_sport_data.address)) {
                return res.status(400).json({
                    errors: {
                        address: 'Should be true or false',
                    },
                });
            }
        }
        if (new_sport_data.extraData) {
            if (new_sport_data.extraData.webpage) {
                if (!validation.valid_webpage(price.extraData.webpage)) {
                    return res.status(400).json({
                        errors: {
                            extraData_webpage: 'Should be in correct format:',
                        },
                    });
                }
            }
        }
        var query = {
            _id: id
        }
        var update = {
            $set: new_sport_data
        };
        var options = {
            new: true
        };
        var callback = (err, sportscenter) => {
            if (err) {
                throw err;
            }
            //res.json(user); //returns the previous user (result of findoneandupdate)
            res.json(sportscenter.toJSON()); //should add checks that not defined in schema
            //return new user data handy for the new form redirection after updated profile
        };
        Sportscenters.findOneAndUpdate(query, update, options, callback);
    }
});
module.exports = router;
