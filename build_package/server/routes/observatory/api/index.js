const express = require('express');
const router = express.Router();

const cors = require('cors');

router.use(cors());
router.all('*', cors());
//router.use('/users', require('./users/users'));
router.use('/products', require('./activities/activities'));
router.use('/shops', require('./sportscenters/sportscenters'));
router.use('/prices', require('./prices/prices'));
router.use('/users', require('./users/users'));
const mongoose = require('mongoose');
const passport = require('passport');
const auth = require('../auth');
const Users = mongoose.model('Users');
const Blacklist = mongoose.model('Blacklists');
const validation = require("../../../astar_modules/validation.js")


router.post('/register', auth.optional, (req, res, next) => {
    /*const {
        body: {
            user
        }
    } = req;
    */
    const user = req.body;
    //check if format is valid (5,6 persons only json)
    if (req.query.format && req.query.format != "json") {
        return res.status(400).json({
            errors: {
                format: 'BAD REQUEST only json for this API',
            },
        });
    }
    if (!user.email || !validation.valid_email(user.email)) {
        return res.status(400).json({
            errors: {
                email: 'is required and in correct format: <sth>@<mail>.<sth>',
            },
        });
    }
    if (!user.password || !validation.valid_password(user.password)) {
        return res.status(400).json({
            errors: {
                password: 'is required and in correct format: 8 to 20 digits',
            },
        });
    }
    if (!user.username) {
        return res.status(400).json({
            errors: {
                username: 'is required',
            },
        });
    }
    if (!user.birth_date || !validation.valid_date(user.birth_date)) {
        return res.status(400).json({
            errors: {
                birth_date: 'is required and in correct format: \'YYYY-MM-DD\'',
            },
        });
    }
    if (!user.first_name || !validation.valid_name(user.first_name)) {
        return res.status(400).json({
            errors: {
                first_name: 'is required',
            },
        });
    }
    if (!user.surname || !validation.valid_name(user.surname)) {
        return res.status(400).json({
            errors: {
                surname: 'is required',
            },
        });
    }
    const finalUser = new Users(user);
    Users.find({
        $or: [{
            "username": user.username
        }, {
            "email": user.email
        }]
    }, function(err, docs) {
        if (docs.length) {
            return res.status(400).json({
                errors: {
                    user: 'user already exists',
                },
            });
        } else {
            finalUser.setPassword(user.password);
            return finalUser.save().then(() => res.json({
                user: finalUser.toAuthJSON()
            }));
        }
    })
});
router.post('/login', auth.optional, (req, res, next) => {
    user = req.body;
    user.email = user.username
    //check if format is valid (5,6 persons only json)
    if (req.query.format && req.query.format != "json") {
        return res.status(400).json({
            errors: {
                format: 'BAD REQUEST only json for this API',
            },
        });
    }
    if (!user.username) {
        return res.status(400).json({
            errors: {
                username: 'is required',
            },
        });
    }
    if (!user.password) {
        return res.status(400).json({
            errors: {
                password: 'is required',
            },
        });
    }
    return passport.authenticate('local', {}, (err, passportUser, info) => {
        if (err) {
            return res.status(403).json({
                errors: "Wrong username or password"
            });
        }
        if (passportUser) {
            const user = passportUser;
            user.token = passportUser.generateJWT();
            return res.json(user.toAuthJSON());
        }
        return status(400).info;
    })(req, res, next);
});
router.post('/logout', auth.required, (req, res, next) => {
    console.log("IN THE LOGOUT")
    //blacklist my friend
    const blacklist_entry = {
        token: req.headers['x-observatory-auth'],
    }
    blacklist_token = new Blacklist(blacklist_entry);
    blacklist_token.save()
    //check if format is valid (5,6 persons only json)
    if (req.query.format && req.query.format != "json") {
        return res.status(400).json({
            errors: {
                format: 'BAD REQUEST only json for this API',
            },
        });
    }
    console.log(req.session)
    req.logOut();
    req.payload = null;
    return res.status(200).json({
        message: "OK",
    });
    //req.logout();
    //req.session.destroy();
    //req.session = null;
    //req.logout();
    //res.redirect('/'); //Send him to the welcome page
});

function check_blacklisted(token, model) {}
module.exports = router;
