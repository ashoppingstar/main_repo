const mongoose = require('mongoose');
const passport = require('passport');
const router = require('express').Router();
const auth = require('../../auth');
const Activities = mongoose.model('Activities');
const Users = mongoose.model('Users'); //for user checking on delete
var cookieParser = require('cookie-parser');
const Blacklist = mongoose.model('Blacklists');


router.post('/', auth.required, (req, res, next) => {


    //verify every move for an auth.required man
    Blacklist.findOne({ token: req.headers['x-observatory-auth']}).then(function (result) {
        if (result) {
            return res.status(422).json({
            errors: {
                user_error: 'login is required',
            },
            });    
        }
        else{
            validated_token();
        }
    });

    const validated_token = () => {

        const activity = req.body;

        //check if format is valid (5,6 persons only json)
        if ( req.query.format && req.query.format != "json") {
            return res.status(400).json({
                errors: {
                    format: 'BAD REQUEST only json for this API',
                },
            });
        }

        if (!activity.name) {
            return res.status(422).json({
                errors: {
                    name: 'is required',
                },
            });
        }
        if (!activity.extraData.price) {
            return res.status(422).json({
                errors: {
                    price: 'is required',
                },
            });
        }
        if (!activity.extraData.schedule) {
            return res.status(422).json({
                errors: {
                    schedule: 'is required',
                },
            });
        }
        if (!activity.extraData.attendance) {
            return res.status(422).json({
                errors: {
                    attendance: 'is required',
                },
            });
        }
        if (!activity.extraData.rating) {
            return res.status(422).json({
                errors: {
                    rating: 'is required',
                },
            });
        }
        if (!activity.extraData.age_group) {
            return res.status(422).json({
                errors: {
                    age_group: 'is required',
                },
            });
        }
        //tag check missing
        /*if (!activity.sportscenter_id) {
            return res.status(422).json({
                errors: {
                    sportscenter_id: 'is required',
                },
            });
        }*/
        const finalActivity = new Activities(activity);
        return finalActivity.save().then(() => res.json(
            finalActivity.toJSON()
        ));
    }
});

//Get list of all activities
router.get('/', auth.optional, (req, res, next) => {
    
    //start = req.query.start?parseInt(req.query.start):0
    if (isNaN(req.query.start) && req.query.start) { //if start is not a num
        
        return res.status(400).json({
            errors: {
                format: 'BAD REQUEST for start, usage ...?start=INT',
            },
        });
    }
    else start = req.query.start?parseInt(req.query.start):0


    if (isNaN(req.query.count) && req.query.count) { //if start is not a num
        
        return res.status(400).json({
            errors: {
                format: 'BAD REQUEST for start, usage ...?count=INT',
            },
        });
    }
    else count = req.query.count?parseInt(req.query.count):20

    if (req.query.status && (req.query.status !='ACTIVE' && req.query.status !='WITHDRAWN' )) { //if start is not a num
        return res.status(400).json({
            errors: {
                format: 'BAD REQUEST for status, usage ...?status=ACTIVE|WITHDRAWN',
            },
        });
    }
    else {
        status = req.query.status?req.query.status:'ACTIVE' //ACTIVE || WITHDRAWN
        if (status=="ACTIVE") {status = false}
        else{
            status=true
        }   
    }


    if (req.query.sort && !/(id|name)\|(DESC|ASC)/.test(req.query.sort)) {
        return res.status(400).json({
            errors: {
                format: 'BAD REQUEST for sort, usage ...?sort=id|ASC.....',
            },
        });
    }
    else{
        sort = (req.query.sort?req.query.sort:"id|DESC").split("|")
        sort_by = sort[0]
        sort_order = sort[1]
        if (sort_by=='id') {sort_by='_id'}
        if (sort_order=='ASC') {sort_order=1}
        else{sort_order=-1}
    }        


    //check if format is valid (5,6 persons only json)
    if ( req.query.format && req.query.format != "json") {
        return res.status(400).json({
            errors: {
                format: 'BAD REQUEST only json for this API',
            },
        });
    }
    
    Activities.find({withdrawn:[status]}).sort({[sort_by]:[sort_order]}).skip(start).limit(count).lean().exec(function (err, activities) {
        
        
    return res.json({
                start: start,
                count: count, 
                total: activities.length,
                products: [
                    activities
                ]
            });

        
        //return res.end(JSON.stringify(activities));
    })
});

//Get info of activity with id:
router.get('/:_id', auth.optional, (req, res, next) => {
    //check if format is valid (5,6 persons only json)
    if ( req.query.format && req.query.format != "json") {
        return res.status(400).json({
            errors: {
                format: 'BAD REQUEST only json for this API',
            },
        });
    }

   return Activities.findById(req.params._id).then((activity) => {
        if (!activity) {
            return res.sendStatus(404);
        }

        return res.json(
            activity.toJSON()
        );
    });
});

//Delete activity with id = _id
router.delete('/:_id', auth.required, (req, res, next) => {
    //check if format is valid (5,6 persons only json)
    if ( req.query.format && req.query.format != "json") {
        return res.status(400).json({
            errors: {
                format: 'BAD REQUEST only json for this API',
            },
        });
    }
    
    //get user id
    const {
        payload: {
            id
        }
    } = req;


    //verify every move for an auth.required man
    Blacklist.findOne({ token: req.headers['x-observatory-auth']}).then(function (result) {
        if (result) {
            return res.status(422).json({
            errors: {
                user_error: 'login is required',
            },
            });    
        }
        else{
            validated_token();
        }
    });



    const validated_token = () => {

        return Users.findById(id).then((user) => {
            if (!user) {
                return res.sendStatus(404);
            }

            if (user.is_admin) {
                var query = {_id: req.params._id};
                Activities.remove(query, (err, activity) => {
                    if(err){
                        throw err;
                    }
                    res.json({message:"OK"});
                });        
            }
            else{
                var query = {_id: req.params._id}
                var update =  {$set:{"withdrawn":true}};
                var options = {};
                var callback = (err, activity) => {
                    if(err){
                        throw err;
                    }
                    //res.json(user); //returns the previous user (result of findoneandupdate)
                    res.json({message:"OK"}); //should add checks that not defined in schema
                    //return new user data handy for the new form redirection after updated profile
                };
                Activities.update(query,update,callback);
            }

          /*  */
        });
    }

        
    /*
        
        
        Activities.remove(query, (err, activity) => {
            if(err){
                throw err;
            }
            res.json(activity);
        });
        */
});


//fULL UPDATE
router.put('/:_id', auth.required, (req, res, next) => {


    //verify every move for an auth.required man
    Blacklist.findOne({ token: req.headers['x-observatory-auth']}).then(function (result) {
        if (result) {
            return res.status(422).json({
            errors: {
                user_error: 'login is required',
            },
            });    
        }
        else{
            validated_token();
        }
    });



    const validated_token = () => {
        //check if format is valid (5,6 only json)
        if ( req.query.format && req.query.format != "json") {
            return res.status(400).json({
                errors: {
                    format: 'BAD REQUEST only json for this API',
                },
            });
        }

        var id = req.params._id;
        var new_act_data = req.body;

        var update = {
                name: new_act_data.name,
                tags: new_act_data.tags,
                withdrawn: new_act_data.withdrawn,
                extraData: new_act_data.extraData,
        }

        var query = {_id: id};
        var options = {};
        var callback = (err, activity) => {
            if(err){
                throw err;
            }
            //res.json(user); //returns the previous user (result of findoneandupdate)
            res.json(new_act_data); //should add checks that not defined in schema
            //return new user data handy for the new form redirection after updated profile
        };
        Activities.findOneAndUpdate(query, update, options, callback);
    }
});


//Update user with _id from user end scope
router.patch('/:_id', auth.required, (req, res, next) => {


    //verify every move for an auth.required man
    Blacklist.findOne({ token: req.headers['x-observatory-auth']}).then(function (result) {
        if (result) {
            return res.status(422).json({
            errors: {
                user_error: 'login is required',
            },
            });    
        }
        else{
            validated_token();
        }
    });



    const validated_token = () => {
        //check if format is valid (5,6 persons only json)
        if ( req.query.format && req.query.format != "json") {
            return res.status(400).json({
                errors: {
                    format: 'BAD REQUEST only json for this API',
                },
            });
        }

        var id = req.params._id;
        var new_act_data = req.body;

        var query = {_id: id}
        var update =  {$set:new_act_data};
        var options = {};
        var callback = (err, activity) => {
            if(err){
                throw err;
            }
            //res.json(user); //returns the previous user (result of findoneandupdate)
            res.json(new_act_data); //should add checks that not defined in schema
            //return new user data handy for the new form redirection after updated profile
        };
        Activities.update(query,update,callback);
    }
});


module.exports = router;