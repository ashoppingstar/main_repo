const mongoose = require('mongoose');
const passport = require('passport');
const router = require('express').Router();
const auth = require('../../auth');
const Activities = mongoose.model('Activities');
const Users = mongoose.model('Users'); //for user checking on delete
var cookieParser = require('cookie-parser');
const Blacklist = mongoose.model('Blacklists');
const all_tags = ['Ομαδικά αθλήματα', 'Ατομικά αθλήματα', 'Πολεμικές τέχνες', 'Χοροί', 'Αθλήματα νερού', 'Δραστηριότητες εσωτερικού χώρου', 'Δραστηριότητες εξωτερικού χώρου', 'Χειμερινά']
const validation = require("../../../../astar_modules/validation.js")
//EXTRA ENDPOINTS
//Get list of activity-names
router.get('/names', auth.optional, (req, res, next) => {
    //status default to false, if we want to take the names of the withdrawn ones: fix
    Activities.find({
        withdrawn: false
    }, {
        'name': 1
    }).sort({
        'name': 1
    }).lean().exec(function(err, activity_names) {
        console.log({
            activity_names
        });
        //Activities.count({withdrawn:false}, function (err,total_count) {
        return res.json({
            activity_names
        });
        //start: start,
        //count: count, 
        //total: total_count,
        //products: [
        //    activities
        //]
        //});
    })
    //})
});
router.post('/', auth.required, (req, res, next) => {
    //verify every move for an auth.required man
    Blacklist.findOne({
        token: req.headers['x-observatory-auth']
    }).then(function(result) {
        if (result) {
            return res.status(403).json({
                errors: {
                    user_error: 'login is required',
                },
            });
        } else {
            validated_token();
        }
    });
    const validated_token = () => {
        const activity = req.body;
        //check if format is valid (5,6 persons only json)
        if (req.query.format && req.query.format != "json") {
            console.log('format')
            return res.status(400).json({
                errors: {
                    format: 'BAD REQUEST only json for this API',
                },
            });
        }
        //only name is for sure required, description-tags: check from frontend
        if (!activity.name || !validation.valid_name(activity.name)) {
            console.log('error name')
            return res.status(400).json({
                errors: {
                    name: 'is required in correct format',
                },
            });
        }
        if (!activity.description || !validation.valid_description(activity.description)) {
            console.log('error description')
            return res.status(400).json({
                errors: {
                    description: 'field is required in correct format (limit: 300 chars)',
                },
            });
        }
        if (!activity.category || !validation.valid_category(activity.category)) {
            console.log('category')
            return res.status(400).json({
                errors: {
                    category: 'field is required in correct format',
                },
            });
        }
        if (!activity.tags || !validation.valid_tags(activity.tags)) {
            console.log(activity.tags)
            if (!activity.tags) {
                console.log(activity.tags)
            }
            console.log('tags')
            return res.status(400).json({
                errors: {
                    tags: 'field is required in correct format',
                },
            });
        }
        /*
        //check milestones
        var user_milestones = {}
        var milestone_achieved = false
        if (activity.tags.includes('ΟΜΑΔΙΚΑ ΑΘΛΗΜΑΤΑ')) {
            user_milestones.teamplayer = true
            milestone_achieved = true
        }

        if (milestone_achieved) {
            var query = {email: req.payload.email}
            var update =  {$set: user_milestones};
            var options = {new: true};
            var callback = (err, user) => {
                if(err){
                    throw err;
                }
            };
            Users.findOneAndUpdate(query, update, options, callback);
        }
        //end milestones
*/
        const finalActivity = new Activities(activity);
        return finalActivity.save().then(() => res.json(finalActivity.toJSON()));
    }
});
//Get list of all activities
router.get('/', auth.optional, (req, res, next) => {
    //start = req.query.start?parseInt(req.query.start):0
    if (isNaN(req.query.start) && req.query.start) { //if start is not a num
        return res.status(400).json({
            errors: {
                format: 'BAD REQUEST for start, usage ...?start=INT',
            },
        });
    } else start = req.query.start ? parseInt(req.query.start) : 0
    if (isNaN(req.query.count) && req.query.count) { //if start is not a num
        return res.status(400).json({
            errors: {
                format: 'BAD REQUEST for start, usage ...?count=INT',
            },
        });
    } else count = req.query.count ? parseInt(req.query.count) : 20
    with_query = {}
    if (req.query.status && (req.query.status != 'ACTIVE' && req.query.status != 'WITHDRAWN' && req.query.status != 'ALL')) {
        return res.status(400).json({
            errors: {
                format: 'BAD REQUEST for status, usage ...?status=ACTIVE|WITHDRAWN',
            },
        });
    } else {
        status = req.query.status ? req.query.status : 'ACTIVE' //ACTIVE || WITHDRAWN
        if (status == "ACTIVE") {
            status = false
            with_query = {
                $and: [{
                    $or: [{
                        withdrawn: false
                    }]
                }]
            }
        } else if (status == "WITHDRAWN") {
            with_query = {
                $and: [{
                    $or: [{
                        withdrawn: true
                    }]
                }]
            }
        } else {
            console.log(status)
        }
    }
    if (req.query.category && (!validation.valid_category(req.query.category))) {
        return res.status(400).json({
            errors: {
                format: 'BAD REQUEST for category, usage ...?status=One of the category names',
            },
        });
    } else {
        category = req.query.category
        if (category) {
            with_query['$and'].push({
                category: category
            })
        } else {
            // statement
        }
    }
    if (req.query.sort && !/^(id|name)\|(DESC|ASC)$/.test(req.query.sort)) {
        return res.status(400).json({
            errors: {
                format: 'BAD REQUEST for sort, usage ...?sort=id|ASC.....',
            },
        });
    } else {
        sort = (req.query.sort ? req.query.sort : "id|DESC").split("|")
        sort_by = sort[0]
        sort_order = sort[1]
        if (sort_by == 'id') {
            sort_by = '_id'
        }
        if (sort_order == 'ASC') {
            sort_order = 1
        } else {
            sort_order = -1
        }
    }
    //check if format is valid (5,6 persons only json)
    if (req.query.format && req.query.format != "json") {
        return res.status(400).json({
            errors: {
                format: 'BAD REQUEST only json for this API',
            },
        });
    }
    Activities.find(with_query).sort({
        [sort_by]: [sort_order]
    }).skip(start).limit(count).lean().exec(function(err, activities) {
        Activities.count(with_query, function(err, total_count) {
            return res.json({
                start: start,
                count: count,
                total: total_count,
                products: activities
            });
        })
    })
});
//Get info of activity with id:
router.get('/recent', auth.optional, (req, res, next) => {
    var category_data = req.body;
    Activities.find().sort({
        ['_id']: [-1]
    }).lean().exec(function(err, activities) {
        console.log(activities)
        var results = {
            team: [],
            solo: [],
            indoors: [],
            outdoors: [],
            ice: [],
            water: [],
            fight: [],
            dance: []
        }
        var connecter = {
            'Ομαδικά αθλήματα': 'team',
            'Ατομικά αθλήματα': 'solo',
            'Δραστηριότητα εσωτερικού χώρου': 'indoors',
            'Δραστηριότητα εξωτερικού χώρου': 'outdoors',
            'Χειμερινά': 'ice',
            'Αθλήματα νερού': 'water',
            'Πολεμικές τέχνες': 'fight',
            'Χοροί': 'dance'
        }
        var count = [0, 0, 0, 0, 0, 0, 0, 0]
        var tags = ['Ομαδικά αθλήματα', 'Ατομικά αθλήματα', 'Πολεμικές τέχνες', 'Χοροί', 'Αθλήματα νερού', 'Δραστηριότητα εσωτερικού χώρου', 'Δραστηριότητα εξωτερικού χώρου', 'Χειμερινά']
        for (var i = 0; i < activities.length; i++) {
            console.log(i)
            if (count.reduce((partial_sum, a) => partial_sum + a) == 40) {
                break
            }
            for (var j = 0; j < activities[i].tags.length; j++) {
                if (tags.includes(activities[i].tags[j]) && count[tags.indexOf(activities[i].tags[j])] < 5) {
                    count[tags.indexOf(activities[i].tags[j])]++
                        results[connecter[activities[i].tags[j]]].push(activities[i])
                    console.log(count)
                    break
                }
            }
        }
        return res.json(results)
    })
});
//autocomplete!!!
router.get('/autocomplete', function(req, res) {
    var regex = new RegExp(req.query["term"], 'i');
    var query = Activities.find({
        $or: [{
            name: regex
        }]
    }, {
        'name': 1,
        '_id': 1
    }).sort({
        "_id": -1
    }).limit(10);
    var buildResultSet = function(docs) {
        var result = [];
        for (var object in docs) {
            result.push(docs[object]);
        }
        return result;
    }
    // Execute query in a callback and return users list
    query.exec(function(err, activities) {
        if (!err) {
            // Method to construct the json result set
            var result = buildResultSet(activities);
            res.send(result, {
                'Content-Type': 'application/json'
            }, 200);
        } else {
            res.send(JSON.stringify(err), {
                'Content-Type': 'application/json'
            }, 404);
        }
    });
});
//Get info of activity with id:
router.get('/:_id', auth.optional, (req, res, next) => {
    //check if format is valid (5,6 persons only json)
    if (req.query.format && req.query.format != "json") {
        return res.status(400).json({
            errors: {
                format: 'BAD REQUEST only json for this API',
            },
        });
    }
    if (!mongoose.Types.ObjectId.isValid(req.params._id)) {
        return res.status(400).json({
            errors: {
                id: 'Invalid id format',
            },
        });
    }
    return Activities.findById(mongoose.Types.ObjectId(req.params._id)).then((activity) => {
        if (!activity) {
            return res.sendStatus(404);
        }
        return res.json(activity.toJSON());
    });
});
//Delete activity with id = _id
router.delete('/:_id', auth.required, (req, res, next) => {
    //check if format is valid (5,6 persons only json)
    if (req.query.format && req.query.format != "json") {
        return res.status(400).json({
            errors: {
                format: 'BAD REQUEST only json for this API',
            },
        });
    }
    //check id format given as parameter
    if (!mongoose.Types.ObjectId.isValid(req.params._id)) {
        return res.status(400).json({
            errors: {
                id: 'Invalid id format',
            },
        });
    }
    //get user id
    const {
        payload: {
            id
        }
    } = req;
    //verify every move for an auth.required man
    Blacklist.findOne({
        token: req.headers['x-observatory-auth']
    }).then(function(result) {
        if (result) {
            return res.status(403).json({
                errors: {
                    user_error: 'login is required',
                },
            });
        } else {
            validated_token();
        }
    });
    const validated_token = () => {
        return Users.findById(id).then((user) => {
            if (!user) {
                return res.sendStatus(404);
            }
            if (user.is_admin) {
                var query = {
                    _id: req.params._id
                };
                Activities.remove(query, (err, activity) => {
                    if (err) {
                        throw err;
                    }
                    res.json({
                        message: "OK"
                    });
                });
            } else {
                var query = {
                    _id: req.params._id
                }
                var update = {
                    $set: {
                        "withdrawn": true
                    }
                };
                var options = {};
                var callback = (err, activity) => {
                    if (err) {
                        throw err;
                    }
                    //res.json(user); //returns the previous user (result of findoneandupdate)
                    res.json({
                        message: "OK"
                    }); //should add checks that not defined in schema
                    //return new user data handy for the new form redirection after updated profile
                };
                Activities.update(query, update, callback);
            }
            /*  */
        });
    }
    /*
        
        
        Activities.remove(query, (err, activity) => {
            if(err){
                throw err;
            }
            res.json(activity);
        });
        */
});
//FULL UPDATE
router.put('/:_id', auth.required, (req, res, next) => {
    //verify every move for an auth.required man
    Blacklist.findOne({
        token: req.headers['x-observatory-auth']
    }).then(function(result) {
        if (result) {
            return res.status(403).json({
                errors: {
                    user_error: 'login is required',
                },
            });
        } else {
            validated_token();
        }
    });
    const validated_token = () => {
        //check if format is valid (5,6 only json)
        if (req.query.format && req.query.format != "json") {
            return res.status(400).json({
                errors: {
                    format: 'BAD REQUEST only json for this API',
                },
            });
        }
        //check id format given as parameter
        if (!mongoose.Types.ObjectId.isValid(req.params._id)) {
            return res.status(400).json({
                errors: {
                    id: 'Invalid id format',
                },
            });
        }
        var id = req.params._id;
        var new_act_data = req.body;
        //check if all parameters exist 
        var missing_field = (!new_act_data.name || !new_act_data.description || new_act_data.withdrawn == null || !new_act_data.category || !new_act_data.tags)
        if (missing_field) {
            return res.status(400).json({
                errors: {
                    update: 'all fields are required for full update. Else use patch endpoint',
                },
            });
        }
        //format checking
        if (!validation.valid_name(new_act_data.name)) {
            return res.status(400).json({
                errors: {
                    name: 'is required',
                },
            });
        }
        if (!validation.valid_tags(new_act_data.tags)) {
            return res.status(400).json({
                errors: {
                    tags: 'undefined tag',
                },
            });
        }
        if (!validation.valid_description(new_act_data.description)) {
            return res.status(400).json({
                errors: {
                    description: 'too many chars (limit: 300 chars)',
                },
            });
        }
        if (!validation.valid_category(new_act_data.category)) {
            return res.status(400).json({
                errors: {
                    category: 'undefined category',
                },
            });
        }
        if (!validation.valid_withdrawn(new_act_data.withdrawn)) {
            return res.status(400).json({
                errors: {
                    withdrawn: 'Should be true or false',
                },
            });
        }
        var update = {
            name: new_act_data.name,
            description: new_act_data.description,
            category: new_act_data.category,
            tags: new_act_data.tags,
            withdrawn: new_act_data.withdrawn,
        }
        var query = {
            _id: id
        };
        var options = {
            new: true
        };
        var callback = (err, activity) => {
            if (err) {
                throw err;
            }
            res.json(activity.toJSON()); //should add checks that not defined in schema
        };
        Activities.findOneAndUpdate(query, update, options, callback);
    }
});
//Update user with _id from user end scope
router.patch('/:_id', auth.required, (req, res, next) => {
    //verify every move for an auth.required man
    Blacklist.findOne({
        token: req.headers['x-observatory-auth']
    }).then(function(result) {
        if (result) {
            return res.status(403).json({
                errors: {
                    user_error: 'login is required',
                },
            });
        } else {
            validated_token();
        }
    });
    const validated_token = () => {
        //check if format is valid (5,6 persons only json)
        if (req.query.format && req.query.format != "json") {
            return res.status(400).json({
                errors: {
                    format: 'BAD REQUEST only json for this API',
                },
            });
        }
        //check id format given as parameter
        if (!mongoose.Types.ObjectId.isValid(req.params._id)) {
            return res.status(400).json({
                errors: {
                    id: 'Invalid id format',
                },
            });
        }
        var id = req.params._id;
        var new_act_data = req.body;
        //format checking
        if (new_act_data.name) {
            if (!validation.valid_name(new_act_data.name)) {
                return res.status(400).json({
                    errors: {
                        name: 'Invalid name given. Use only greek chars and at least one word',
                    },
                });
            }
        }
        //check if given tags are from the specified tags-list
        if (new_act_data.tags) {
            if (!validation.valid_tags(new_act_data.tags)) {
                return res.status(400).json({
                    errors: {
                        tags: 'undefined tag',
                    },
                });
            }
        }
        //descr is up limited by 300 chars
        if (new_act_data.description) {
            if (!validation.valid_description(new_act_data.description)) {
                return res.status(400).json({
                    errors: {
                        description: 'too many chars (limit: 300 chars)',
                    },
                });
            }
        }
        //check if given category is from the specified tags-list
        if (new_act_data.category) {
            if (!validation.valid_category(new_act_data.category)) {
                return res.status(400).json({
                    errors: {
                        category: 'undefined category',
                    },
                });
            }
        }
        if (new_act_data.withdrawn) {
            if (!validation.valid_withdrawn(new_act_data.withdrawn)) {
                return res.status(400).json({
                    errors: {
                        description: 'withdrawn should be true or false',
                    },
                });
            }
        }
        var query = {
            _id: id
        }
        var update = {
            $set: new_act_data
        };
        var options = {
            new: true
        };
        var callback = (err, activity) => {
            if (err) {
                throw err;
            }
            res.json(activity.toJSON()); //should add checks that not defined in schema
        };
        Activities.findOneAndUpdate(query, update, options, callback);
        //Activities.update(query,update,options,callback);
    }
});
module.exports = router;