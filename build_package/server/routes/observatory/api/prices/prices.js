const mongoose = require('mongoose');
const passport = require('passport');
const router = require('express').Router();
const auth = require('../../auth');
const Prices = mongoose.model('Prices');
const Activities = mongoose.model('Activities');
const Sportscenters = mongoose.model('Sportscenters');
const ActivitySportscenters = mongoose.model('ActivitySportscenters');
var cookieParser = require('cookie-parser');
const Blacklist = mongoose.model('Blacklists');
const {
    ObjectId
} = require('mongodb');
//const all_tags = ['Ομαδικά αθλήματα', 'Ατομικά αθλήματα', 'Πολεμικές τέχνες', 'Χοροί', 'Αθλήματα νερού', 'Δραστηριότητες εσωτερικού χώρου', 'Δραστηριότητες εξωτερικού χώρου', 'Χειμερινά']
const validation = require("../../../../astar_modules/validation.js")
const milestones = require("../../../../astar_modules/milestones.js")
//EXTRA QUERY, returns rating and schedule from activitysportscenter collection
//Give shop and product id in url query, as in prices.
//BUT: try it  with empty url query or with only one pair shop=...&product=...
router.get('/activitysportscenter', auth.optional, (req, res, next) => {
    if (!req.query.shop || !req.query.product) { //if none or only one of the 2 id's given in url query, return all activsportsc
        ActivitySportscenters.find().exec(function(err, activitysportscenters) {
            console.log({
                activitysportscenters
            });
            return res.json({
                activitysportscenters
            });
        })
    } else { //both product and shop given in url query
        if (!mongoose.Types.ObjectId.isValid(req.query.product) || !mongoose.Types.ObjectId.isValid(req.query.shop)) {
            return res.status(400).json({
                errors: {
                    shop_product: 'id in parameters should be in valid format: 24-digit-string',
                },
            });
        } else {
            match_object = {} //initialize object for match pipeline stage
            match_object['$and'] = [] // all conditions will be feeded in and 
            aggregate_array = [];
            match_object['$and'].push({
                'sportscenter_id': ObjectId(req.query.shop)
            })
            match_object['$and'].push({
                'activity_id': ObjectId(req.query.product)
            })
            aggregate_array.push({
                '$match': match_object
            })
            //console.log(aggregate_array)
            ActivitySportscenters.aggregate(aggregate_array, function(err, activitysportscenter) {
                if (err) {
                    return res.status(400).json({
                        errors: {
                            database_error: 'SOMETHING GONE REALLY WRONG',
                        },
                    });
                }
                return res.json({
                    activitysportscenters: [
                        activitysportscenter
                    ]
                });
            })
        }
    }
});
router.post('/', auth.required, (req, res, next) => {
    //verify every move for an auth.required man
    Blacklist.findOne({
        token: req.headers['x-observatory-auth']
    }).then(function(result) {
        if (result) {
            return res.status(403).json({
                errors: {
                    user_error: 'login is required',
                },
            });
        } else {
            validated_token();
        }
    });
    //so you are authenticated sir?
    const validated_token = () => {
        const price = req.body;
        price.sportscenter_id = price.shopId;
        price.activity_id = price.productId;
        price.date_from = price.dateFrom;
        price.date_to = price.dateTo;
        price.extraData = (price.extraData == null ? {} : price.extraData);
        //check if format is valid (5,6 persons only json)
        if (req.query.format && req.query.format != "json") {
            return res.status(400).json({
                errors: {
                    format: 'BAD REQUEST only json for this API',
                },
            });
        }
        if (!price.productId || !mongoose.Types.ObjectId.isValid(price.productId)) {
            return res.status(400).json({
                errors: {
                    activity_id: 'is required and in correct format (24-digit-string)',
                },
            });
        }
        if (!price.shopId || !mongoose.Types.ObjectId.isValid(price.shopId)) {
            return res.status(400).json({
                errors: {
                    sportscenter_id: 'is required and in correct format: 24-digit-string',
                },
            });
        }
        if (!price.price || !validation.valid_price(price.price)) {
            return res.status(400).json({
                errors: {
                    price: 'is required and in correct format: number in [1, 1000]',
                },
            });
        }
        if (price.date_from) {
            if (!validation.valid_date(price.date_from)) {
                return res.status(400).json({
                    errors: {
                        dateFrom: 'Should be in correct format: \'YYYY-MM-DD\'',
                    },
                });
            }
        }
        if (price.date_to) {
            if (!validation.valid_date(price.date_to)) {
                return res.status(400).json({
                    errors: {
                        dateTo: 'Should be in correct format: \'YYYY-MM-DD\'',
                    },
                });
            }
        }
        if (price.extraData) {
            if (price.extraData.rating) {
                if (!validation.valid_rating(price.extraData.rating)) {
                    return res.status(400).json({
                        errors: {
                            extraData_rating: 'Should be in correct format: int in [1,5]',
                        },
                    });
                }
            }
            if (price.extraData.schedule) {
                if (!validation.valid_schedule(price.extraData.schedule)) {
                    return res.status(400).json({
                        errors: {
                            extraData_schedule: 'Should be in correct format: mon:[[Number]], tus:[[Number]]...',
                        },
                    });
                }
            }
        }
        var global_activity = {}
        var callback_check_activity = (err, activity, randomer) => {
            if (err) {
                throw err;
            }
            if (!activity) {
                return res.sendStatus(404);
            }
            global_activity = activity
            //console.log("CALLBACK 1")
            //Check if sportscenter exists. If not, it should be created first
            Sportscenters.findById(mongoose.Types.ObjectId(price.shopId), callback_check_sportscenter)
        }
        var callback_check_sportscenter = (err, sportscenter) => {
            if (err) {
                throw err;
            }
            if (!sportscenter) {
                return res.sendStatus(404);
            }
            //console.log("CALLBACK 2")
            //About AvtivitySportscenter
            var query = {
                activity_id: mongoose.Types.ObjectId(price.productId),
                sportscenter_id: mongoose.Types.ObjectId(price.shopId)
            };
            var update = {
                $set: {
                    price: price.price
                }
            };
            var options = {
                new: true
            };
            //give our man his credits! after having the activity and sportscenter infos
            milestones.update(req.payload.email, global_activity, sportscenter)
            //Keep the last inserted price for this activity-sportscenter
            ActivitySportscenters.findOneAndUpdate(query, update, options, callback_check_activitySportscenter)
        }
        //Check if activity exists. If not, it should be created first
        Activities.findById(mongoose.Types.ObjectId(price.productId), callback_check_activity)
        var callback_check_activitySportscenter = (err, activitySportscenter) => {
            if (err) {
                throw err;
            } else if (!activitySportscenter) { //if there isn't such a act-sportsc, create one
                //if no rating came accept but do not for a new rating and initialize to zero for the others
                var in_rating_cnt = 0;
                var in_rating = 0;
                if (price.extraData.rating != null) {
                    in_rating_cnt = 1
                    in_rating = price.extraData.rating
                }
                var empty_schedule = {
                    "mon": [],
                    "tue": [],
                    "wed": [],
                    "thu": [],
                    "fri": [],
                    "sat": [],
                    "sun": []
                }
                if (price.extraData.schedule == null) {
                    price.extraData.schedule = empty_schedule
                }
                var insert_query = {
                    activity_id: mongoose.Types.ObjectId(price.productId),
                    sportscenter_id: mongoose.Types.ObjectId(price.shopId),
                    price: price.price,
                    extraData: {
                        rating: in_rating,
                        rating_counter: in_rating_cnt,
                        schedule: unify_schedules(empty_schedule, price.extraData.schedule)
                    }
                };
                const finalActSp = new ActivitySportscenters(insert_query);
                finalActSp.save(callback_update_relative)
            } else { //else complete the update (attendance, rating, rating_counter)
                //SCHEDULE!!!
                if (!price.extraData.rating) {
                    in_rating_cnt = 0
                    in_rating = 0
                } else {
                    var in_rating_cnt = 1;
                    var in_rating = parseInt(price.extraData.rating, 10);;
                }
                var activitySportscenterJson = activitySportscenter.toJSON();
                var new_rating_counter = activitySportscenterJson.extraData.rating_counter + in_rating_cnt;
                console.log(new_rating_counter)
                console.log(activitySportscenterJson.extraData.rating_counter)
                console.log(activitySportscenterJson.extraData.rating)
                console.log(in_rating)
                if (new_rating_counter == 0) {
                    new_rating = 0
                } else {
                    var new_rating = (activitySportscenterJson.extraData.rating_counter * activitySportscenterJson.extraData.rating + in_rating) / new_rating_counter;
                    console.log(new_rating)
                }
                var new_attendance = 0 //attendance according to how many users rated the act-sp
                if (new_rating_counter < 5) {
                    new_attendance = 0 //low attendance
                } else if (new_rating_counter < 10) {
                    new_attendance = 1 //medium attendance
                } else {
                    new_attendance = 2 //high attedance
                }
                var query = {
                    activity_id: mongoose.Types.ObjectId(price.productId),
                    sportscenter_id: mongoose.Types.ObjectId(price.shopId)
                };
                if (!price.extraData.schedule) {
                    var update = {
                        $set: {
                            extraData: {
                                attendance: new_attendance,
                                rating: new_rating,
                                rating_counter: new_rating_counter,
                                schedule: activitySportscenterJson.extraData.schedule,
                                create_date: activitySportscenterJson.extraData.create_date
                            }
                        }
                    };
                } else {
                    //construct new schedule
                    var new_schedule = unify_schedules(activitySportscenterJson.extraData.schedule, price.extraData.schedule)
                    var update = {
                        $set: {
                            extraData: {
                                attendance: new_attendance,
                                rating: new_rating,
                                rating_counter: new_rating_counter,
                                schedule: new_schedule,
                                create_date: activitySportscenterJson.extraData.create_date
                            }
                        }
                    };
                }
                var options = {
                    new: true
                };
                ActivitySportscenters.findOneAndUpdate(query, update, options, callback_update_relative)
            }
        }
        var callback_update_relative = (err, result) => {
            if (err) {
                throw err;
            }
            var price_query = {
                activity_id: mongoose.Types.ObjectId(price.productId),
                sportscenter_id: mongoose.Types.ObjectId(price.shopId),
                price: price.price,
                date_to: price.date_to,
                date_from: price.date_from,
                user: req.payload.email //keeep who did all this 
            };
            var date_array = getDateArray(new Date(price_query.date_from), new Date(price_query.date_to))
            prices_arr = []
            prices_arr = date_array.map(function(x) {
                var in_price = {
                    activity_id: price_query.activity_id,
                    sportscenter_id: price_query.sportscenter_id,
                    price: price_query.price,
                    date: x,
                    date_from: price_query.date_from,
                    date_to: price_query.date_to,
                    user: price_query.user //keeep who did all this 
                };
                return in_price
            });
            Prices.insertMany(prices_arr).then((result) => {
                res.status(200).json({
                    "start": 0,
                    "total": date_array.length,
                    "count": date_array.length,
                    "prices": result
                });
            }).catch(err => {
                console.error("error ", err);
                res.status(400).json({
                    err
                });
            });
            //construct multiple prices for date range
            //const finalPrice = new Prices(price_query);
            //return finalPrice.save().then(() => res.json(finalPrice.toJSON()));
        };
        /*var callback = (err, activitySportscenter) => {
            if(err){
                throw err;
            }
            //res.json(activitySportscenter.toJSON()); //should add checks that not defined in schema
        };*/
        //About Prices
        //oxi, prepei na valoume sygkekrimena pedia apo to price.
        //const finalPrice = new Prices(price);
    }
});
// date array
var getDateArray = function(start, end) {
    var arr = new Array(),
        dt = new Date(start);
    while (dt <= end) {
        arr.push(new Date(dt));
        dt.setDate(dt.getDate() + 1);
    }
    return arr;
}
//Get list of prices
router.get('/', auth.optional, (req, res, next) => {
    if (isNaN(req.query.start) && req.query.start) { //if start is not a num
        return res.status(400).json({
            errors: {
                format: 'BAD REQUEST for start, usage ...?start=INT',
            },
        });
    } else start = req.query.start ? parseInt(req.query.start) : 0
    if (isNaN(req.query.count) && req.query.count) { //if start is not a num
        return res.status(400).json({
            errors: {
                format: 'BAD REQUEST for count, usage ...?count=INT',
            },
        });
    } else count = req.query.count ? parseInt(req.query.count) : 20
    if (req.query.geoLng && req.query.geoLat && req.query.geoDist) { //if all are given
        if (isNaN(req.query.geoLng)) return res.status(400).json({
            errors: {
                format: 'BAD REQUEST for geoLng, usage ...?geoLng=DOUBLE',
            },
        });
        else if (isNaN(req.query.geoLat)) return res.status(400).json({
            errors: {
                format: 'BAD REQUEST for geoLat, usage ...?geoLat=DOUBLE',
            },
        });
        else if (isNaN(req.query.geoDist)) return res.status(400).json({
            errors: {
                format: 'BAD REQUEST for geoDist, usage ...?geoDist=INT',
            },
        });
        else { //if all of them are properly given
            var geoLng = parseFloat(req.query.geoLng)
            var geoLat = parseFloat(req.query.geoLat)
            var geoDist = parseInt(req.query.geoDist)
            //query gia na parw shops pou paexoun geoDist apo to stoxo or flagggg
        }
    } else if (!(req.query.geoLng) && !(req.query.geoLat) && !(req.query.geoDist)) { //none of the geoLng, geoLat, geoDist are given so just ignore
        ;
    } else { //one or two of them are given
        return res.status(400).json({
            errors: {
                format: 'BAD REQUEST, none or all of the geoDist, geoLat, geoLng must be given',
            },
        });
    }
    if (req.query.dateFrom && req.query.dateTo) { //if all are given
        if (!validation.valid_date(req.query.dateFrom)) return res.status(400).json({
            errors: {
                format: 'BAD REQUEST for dateFrom, usage ...?dateFrom=YYYY-MM-DD',
            },
        });
        else if (!validation.valid_date(req.query.dateTo)) return res.status(400).json({
            errors: {
                format: 'BAD REQUEST for dateTo, usage ...?dateTo=YYYY-MM-DD',
            },
        });
        else { //if all of them are properly given
            date_from = Date.parse(req.query.dateFrom)
            date_to = Date.parse(req.query.dateTo)
        }
    } else if (req.query.dateTo || req.query.dateFrom) { //one of the dateFrom-To are given so error msg
        return res.status(400).json({
            errors: {
                format: 'BAD REQUEST, none or both dateTo, dateFrom must be given',
            },
        });
    } else { //else none of them are given so ingnore        
        var date_from_temp = new Date(Date.now());
        date_from = Date.parse(String(date_from_temp.getFullYear()) + "-" + String(date_from_temp.getMonth() + 1) + "-" + String(date_from_temp.getDay()))
        date_to = date_from + 1 * 24 * 3600 * 1000
    }
    if (req.query.shops) { //list of shop-id's (number in string form)
        shops = String(req.query.shops).split(",")
        for (shop of shops) {
            if (!mongoose.Types.ObjectId.isValid(shop)) {
                return res.status(400).json({
                    errors: {
                        shop: 'shops id in parameters should be in valid format: 24-digit-string',
                    },
                });
            }
        }
    } else shops = ""
    if (req.query.products) {
        products = String(req.query.products).split(",")
        for (product of products) {
            if (!mongoose.Types.ObjectId.isValid(product)) {
                return res.status(400).json({
                    errors: {
                        product: 'products id in parameters should be in valid format: 24-digit-string',
                    },
                });
            }
        }
    } else products = ""
    if (req.query.tags) {
        tags = String(req.query.tags).split(",")
        if (!validation.valid_tags(tags)) {
            return res.status(400).json({
                errors: {
                    tags: 'tags in parameters should be in valid format: choose elements from all_tags array',
                },
            });
        }
    } else tags = ""
    if (req.query.sort && !/^(price|geoDist|date)\|(DESC|ASC)$/.test(req.query.sort)) {
        console.log('bad sort')
        return res.status(400).json({
            errors: {
                format: 'BAD REQUEST for sort, usage ...?sort=id|ASC.....',
            },
        });
    } else {
        sort = (req.query.sort ? req.query.sort : "price|ASC").split("|")
        sort_by = sort[0]
        sort_order = sort[1]
        if (sort_by == 'geoDist') {
            sort_by = 'shopDist'
        }
        if (sort_by == 'date') {
            sort_by = 'date'
        } //we can change it to current_date or date_to
        if (sort_order == 'ASC') {
            sort_order = 1
        } else {
            sort_order = -1
        }
    }
    //check if format is valid (5,6 persons only json)
    if (req.query.format && req.query.format != "json") {
        return res.status(400).json({
            errors: {
                format: 'BAD REQUEST only json for this API',
            },
        });
    }
    is_first_one = true
    shops_len = shops.length
    products_len = products.length
    tags_len = tags.length
    match_object = {} //initialize object for match pipeline stage
    match_object['$and'] = [] // all conditions will be feeded in and 
    aggregate_array = [];
    //aggregate eveyrthing please (prices/shops/products for required info)
    //ACTIVITIES
    var aggregate_activities = {
        $lookup: {
            from: 'activities',
            localField: 'activity_id',
            foreignField: '_id',
            as: 'product_details'
        }
    }
    var aggregate_act_unwind = {
        $unwind: '$product_details'
    }
    aggregate_array.push(aggregate_activities)
    aggregate_array.push(aggregate_act_unwind)
    //SHOPS
    var aggregate_sportscenters = {
        $lookup: {
            from: 'sportscenters',
            localField: 'sportscenter_id',
            foreignField: '_id',
            as: 'shop_details'
        }
    }
    var aggregate_spor_unwind = {
        $unwind: '$shop_details'
    }
    aggregate_array.push(aggregate_sportscenters)
    aggregate_array.push(aggregate_spor_unwind)
    //first append TAG queries. There we should join with activities
    if (tags_len != 0) {
        tags_object = {}
        tags_object['$or'] = []
        for (var i = 0; i < tags_len; i++) {
            tags_object['$or'].push({
                'product_details.tags': tags[i]
            })
        }
        match_object['$and'].push(tags_object)
    }
    //then those shops in tha list
    if (shops_len != 0) {
        shops_object = {}
        shops_object['$or'] = []
        for (var i = 0; i < shops_len; i++) {
            shops_object['$or'].push({
                'sportscenter_id': ObjectId(shops[i])
            })
        }
        match_object['$and'].push(shops_object)
    }
    //then those products in tha list
    if (products_len != 0) {
        //aggregate_array.push({"$addFields": { "activity_id": {"$toObjectId":"$activity_id"}}})
        products_object = {}
        products_object['$or'] = []
        for (var i = 0; i < products_len; i++) {
            products_object['$or'].push({
                'activity_id': ObjectId(products[i])
            })
        }
        match_object['$and'].push(products_object)
    }
    if (date_from && date_to) {
        date_obj = {}
        date_obj['$and'] = []
        date_obj['$and'].push({
            'date': {
                $gte: new Date(date_from)
            }
        })
        date_obj['$and'].push({
            'date': {
                $lte: new Date(date_to)
            }
        })
        match_object['$and'].push(date_obj)
    }
    //date parameters
    aggregate_array.push({
        '$match': match_object
    })
    Prices.aggregate(aggregate_array, function(err, prices) {
        if (err) {
            return res.status(400).json({
                errors: {
                    database_error: 'SOMETHING GONE REALLY WRONG',
                },
            });
        }
        //avoid the return and do the geolocation magic on the results please
        if (geoLng != null && geoLat != null && geoDist != null) {
            var circle_turf = require('@turf/circle');
            var booleanWithin = require('@turf/boolean-within');
            var turf = require('@turf/helpers');
            var center = [geoLat, geoLng];
            var radius = geoDist;
            var options = {
                steps: 64,
                units: 'kilometers'
            };
            var circle = circle_turf(center, radius, options);
            var prices = prices.filter(function(el) {
                var point = turf.point([parseFloat(el.shop_details.lat), parseFloat(el.shop_details.lng)])
                return booleanWithin(point, circle)
            });
            //add distance
            var distance = require('@turf/distance');
            var prices = prices.map(function(price) {
                price.shopDist = distance.default(turf.point([price.shop_details.lat, price.shop_details.lng]), turf.point([geoLat, geoLng]), {
                    units: 'kilometers'
                })
                return price
            })
        }
        if (sort_by == "price" || sort_by == "geo.dist") {
            prices.sort((a, b) => (sort_order) * (parseFloat((a[sort_by])) - parseFloat(b[sort_by])));
        } else {
            prices.sort((a, b) => (sort_order) * (((new Date(a[sort_by])).valueOf()) - (new Date(b[sort_by])).valueOf()));
        }
        var paginated_prices = prices.slice(start, count + 1)
        var reduced_paginated_prices = paginated_prices.map(function(price) {
            var reduced_price = {
                "price": price.price,
                "date": price.date.toISOString().split('T')[0],
                "dateTo": price.date_to,
                "dateFrom": price.date_from,
                "productName": price.product_details.name,
                "productId": price.product_details._id,
                "productTags": price.product_details.tags,
                "shopName": price.shop_details.name,
                "shopId": price.shop_details._id,
                "shopTags": price.shop_details.tags,
                "shopAddress": price.shop_details.address,
                "shopDist": price.shopDist,
                "user": price.user
            }
            return reduced_price
        });
        //var point = turf.point([75.343, 20.982]);
        //console.log(booleanWithin(point,circle))
        reduced_paginated_prices = reduced_paginated_prices.map(function(x) {
            x.id = x["_id"]
            return x
        });
        return res.json({
            start: start,
            count: count,
            total: prices.length,
            prices: reduced_paginated_prices
        });
    })
    //Prices.find(json_query).sort({[sort_by]:[sort_order]}).skip(start).limit(count).lean().exec(function (err, prices) {
    //edw epalithesi twn tags. apo to antikeimeno epistrofhs (prices) dialegw ta activities pou exoun ta tags pou thelw
    //return res.end(JSON.stringify(activities));
});

function unify_schedules(a, b) {
    var new_schedule = {}
    if (!b) {
        b = {
            "mon": [],
            "tue": [],
            "wed": [],
            "thu": [],
            "fri": [],
            "sat": [],
            "sun": []
        }
    }
    new_schedule.mon = unier(a.mon, b.mon)
    new_schedule.tus = unier(a.tus, b.tus)
    new_schedule.wed = unier(a.wed, b.wed)
    new_schedule.thu = unier(a.thu, b.thu)
    new_schedule.fri = unier(a.fri, b.fri)
    new_schedule.sat = unier(a.sat, b.sat)
    new_schedule.sun = unier(a.sun, b.sun)
    return new_schedule
}

function unier(older, newer) {
    if (!older && !newer) {
        return [];
    }
    if (!older) {
        newer.sort((a, b) => ((a[0]) - (b[0])));
        return remove_duplicates_safe(newer)
    }
    if (!newer) {
        older.sort((a, b) => ((a[0]) - (b[0])));
        return remove_duplicates_safe(older)
    }
    var temp = older.concat(newer);
    for (var j = 0; j < temp.length; j++) {
        it = 0;
        for (var i = 0; i < temp.length; i++) {
            if (temp[j][0] <= temp[i][1] && temp[j][1] >= temp[i][0]) {
                temp[it] = [Math.min(temp[j][0], temp[i][0]), Math.max(temp[j][1], temp[i][1])]
            }
            it++;
        }
    }
    temp.sort((a, b) => ((a[0]) - (b[0])));
    return remove_duplicates_safe(temp);
}

function remove_duplicates_safe(arr) {
    var seen = {};
    var ret_arr = [];
    for (var i = 0; i < arr.length; i++) {
        if (!(arr[i] in seen)) {
            ret_arr.push(arr[i]);
            seen[arr[i]] = true;
        }
    }
    return ret_arr
}
module.exports = router;