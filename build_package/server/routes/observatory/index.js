const express = require('express');
const router = express.Router();

const cors = require('cors');

router.use(cors());
router.all('*', cors());

router.use('/api', require('./api')); //prefix login with api

router.get('/', function (req, res) {
  if (req.session.tokener == null) {
    res.send('You are not registered in sir')
  }
  else{
    res.send('Come on registered user');  
  }
  
});

module.exports = router;