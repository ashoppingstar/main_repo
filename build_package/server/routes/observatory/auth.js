const mongoose = require('mongoose');
const jwt = require('express-jwt');
const Blacklist = mongoose.model('Blacklists');

const getTokenFromHeaders =  (req) => {
  //const { headers: { authorization } } = req;

  const authorization = req.headers['x-observatory-auth']

  //check in the blacklisted database
  //if it exists return null 
  //else return the authorization

  return authorization
};


const auth = {
  required: jwt({
    secret: 'secret',
    userProperty: 'payload',
    getToken: getTokenFromHeaders,
  }),
  optional: jwt({
    secret: 'secret',
    userProperty: 'payload',
    getToken: getTokenFromHeaders,
    credentialsRequired: false,
  }),
};

module.exports = auth;