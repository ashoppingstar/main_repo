const mongoose = require('mongoose');
const jwt = require('jsonwebtoken');

const { Schema } = mongoose;

const SportscentersSchema = new Schema({
 name:{
    type: String,
    required: true
  },
  address:{
    type: String,
  },
  lng:{
    type: Number,
  },
  lat:{
    type: Number,
  },
  tags: [String],
  withdrawn:{
    type: Boolean,
    default: false
  },
  extraData:{
      webpage:{
        type: String,
        required: false
      },
      create_date:{
        type: Date,
        default: Date.now
      },
  },
});

SportscentersSchema.methods.toJSON = function() {
  return {
    id: this._id,
    name: this.name,
    address: this.address,
    lng: this.lng,
    lat: this.lat,
    tags: this.tags,
    withdrawn: this.withdrawn,
    extraData: this.extraData,
  };
};

mongoose.model('Sportscenters', SportscentersSchema);