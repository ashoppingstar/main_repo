const mongoose = require('mongoose');
const crypto = require('crypto');
const jwt = require('jsonwebtoken');

const { Schema } = mongoose;

const BlacklistSchema = new Schema({
  token:{
    type: String,
    required: true
  },
  expirationDate:{
    type: Date,
    default: Date.now
  },
});


mongoose.model('Blacklists', BlacklistSchema);