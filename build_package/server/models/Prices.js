const mongoose = require('mongoose');
const jwt = require('jsonwebtoken');

const { Schema } = mongoose;

const PricesSchema = new Schema({
	  activity_id:{
	    type: Schema.ObjectId
	    //required: true
   	},
   	sportscenter_id:{
      type: Schema.ObjectId
      //required: true
    },
    price:{
      type: Number,  
    },
    date_from:{
      type: Date,
      default: Date.now
    },
    date_to:{
      type: Date,
      default: Date.now
    },
    date:{
      type: Date,
      default: Date.now
    },
    current_date:{
      type: Date,
      default: Date.now
    },
    user:{
      type: String,
    }
});

//there we should query for the rest fields needed
PricesSchema.methods.toJSON = function() {
  return {
    id: this._id,
    shopId: this.sportscenter_id,
    productId: this.activity_id,
    price: this.price,
    date: this.date.toISOString().split('T')[0],
    dateFrom: this.date_from,
    dateTo: this.date_to,
    current_date: this.current_date,
    user: this.user
  };
};


mongoose.model('Prices', PricesSchema);