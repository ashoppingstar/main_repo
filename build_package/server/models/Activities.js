const mongoose = require('mongoose');
const jwt = require('jsonwebtoken');
const {
    Schema
} = mongoose;
const ActivitiesSchema = new Schema({
    name: {
        type: String,
        required: true
    },
    description: {
        type: String,
        required: true
    },
    tags: {
        type: [String],
        required: true
    },
    category: {
        type: String,
        required: true
    },
    withdrawn: {
        type: Boolean,
        default: false
    }
});
ActivitiesSchema.methods.toJSON = function() {
    return {
        id: this._id,
        name: this.name,
        description: this.description,
        tags: this.tags,
        category: this.category,
        withdrawn: this.withdrawn,
    };
};
mongoose.model('Activities', ActivitiesSchema);