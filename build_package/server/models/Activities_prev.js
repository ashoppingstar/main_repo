const mongoose = require('mongoose');
const jwt = require('jsonwebtoken');

const { Schema } = mongoose;

const ActivitiesSchema = new Schema({
  name:{
    type: String,
    required: true
  },
  description:{
    type: String,
  },
  tags: [String],  //category req field is tags[0]
  withdrawn:{
    type: Boolean,
    default: false
  },
  extraData:{
    sportscenter_id:{
      type: Schema.ObjectId
      //required: true
    },
    create_date:{
      type: Date,
      default: Date.now
    },
    price:{
      type: Object,
      properies:{
        per_day: Number,
        per_month: Number,
        per_year: Number,  
      },
      
      //required: true
    },
    schedule:{ //strings?? required??
      type: Object,
      properies:{
       mon: [[Number]],
       tus: [[Number]], 
       wed: [[Number]],
       thu: [[Number]],
       fri: [[Number]],
       sat: [[Number]],
       sun: [[Number]],
     },
       //required: true
    },
    attendance:{
      type: String
    },
    rating:{
      type: Number
    },
    age_group:{
      type: String
    },
  },
});

ActivitiesSchema.methods.toJSON = function() {
  return {
    _id: this._id,
    name: this.name,
    tags: this.tags,
    withdrawn: this.withdrawn,
    extraData: this.extraData,
  };
};


mongoose.model('Activities', ActivitiesSchema);