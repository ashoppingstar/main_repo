const mongoose = require('mongoose');
const jwt = require('jsonwebtoken');

const { Schema } = mongoose;

const ActivitySportscentersSchema = new Schema({
  withdrawn:{
    type: Boolean,
    default: false
  },
  sportscenter_id:{
    type: Schema.ObjectId
    //required: true
  },
  activity_id:{
    type: Schema.ObjectId
    //required: true
  },
  price:{
    type: Number  
  },
  extraData:{
    create_date:{
      type: Date,
      default: Date.now
    },
    schedule:{ //strings?? required??
      type: Object,
      properies:{
       mon: [[Number]],
       tus: [[Number]], 
       wed: [[Number]],
       thu: [[Number]],
       fri: [[Number]],
       sat: [[Number]],
       sun: [[Number]],
     },
     default:{
       mon: [],
       tus: [], 
       wed: [],
       thu: [],
       fri: [],
       sat: [],
       sun: [],

       //required: true
     }
    },
    attendance:{
      type: Number,
      default: 0
    },
    rating:{
      type: Number
    },
    rating_counter:{
      type: Number,
      default: 1
    },
  },
});

ActivitySportscentersSchema.methods.toJSON = function() {
  return {
    _id: this._id,
    withdrawn: this.withdrawn,
    sportscenter_id: this.sportscenter_id,
    activity_id: this.activity_id,
    price: this.price,
    extraData: this.extraData
  };
};


mongoose.model('ActivitySportscenters', ActivitySportscentersSchema);