const mongoose = require('mongoose');
const { Schema } = mongoose;

const MetadataSchema = new Schema({
    email:{
        type: String,
        required: true
    },
    action:{
        type: String,
        required: true
    },
    create_date:{
        type: Date,
        default: Date.now
    },
});


mongoose.model('Metadatas', MetadataSchema);