const mongoose = require('mongoose');
const crypto = require('crypto');
const jwt = require('jsonwebtoken');

const { Schema } = mongoose;

const UsersSchema = new Schema({
  first_name:{
    type: String,
    required: true
  },
  surname:{
    type: String,
    required: true
  },
  birth_date:{
    type: Date
  },
  email:{
    type: String,
    required: true
  },
  username:{
    type: String,
    required: true
  },
  create_date:{
    type: Date,
    default: Date.now
  },
  salt:{
    type: String,
  },
  hash:{
    type: String,
  },
  is_admin:{
    type: Boolean,
    default: false
  },
  achievements:{
    type: Object,
    properties:{ 
      amphibious: Boolean,
      teamplayer: Boolean,
      colombus: Boolean,
      explorer: Boolean,
      allaround: Boolean,
      multi_sportscenters: Boolean,  
      level: String,
    },
    default:{
      amphibious: false,
      teamplayer: false,
      colombus: false,
      explorer: false,
      allaround: false,
      multi_sportscenters: false,
      level: "novice", 
      /*
      Commits  |  level
      1 - 3    |  novice (starting from the bottom)
      4 - 6    |  apprentice
      7 - 9    |  athlete_star
      10 - 12  |  ambassador (gyrologos - polytechnitis)
      */  
    }
  },
  metadata:{
    type: Object,
    properties:{
      commits_number: Number,
    },
    default:{
      commits_number: 0, 
    } 
  }
});

UsersSchema.methods.setPassword = function(password) {
  this.salt = crypto.randomBytes(16).toString('hex');
  this.hash = crypto.pbkdf2Sync(password, this.salt, 10000, 512, 'sha512').toString('hex');
  this.password = this.hash;
};


UsersSchema.methods.validatePassword = function(password) {
  const hash = crypto.pbkdf2Sync(password, this.salt, 10000, 512, 'sha512').toString('hex');
  return this.hash === hash;
};

UsersSchema.methods.generateJWT = function() {
  const today = new Date();
  const expirationDate = new Date(today);
  expirationDate.setDate(today.getDate() + 60);

  return jwt.sign({
    email: this.email,
    id: this._id,
    exp: parseInt(expirationDate.getTime() / 1000, 10),
  }, 'secret');
}

UsersSchema.methods.toAuthJSON = function() {
  return {
    _id: this._id,
    email: this.email,
    birth_date: this.birth_date,
    first_name: this.first_name,
    surname: this.surname,
    username: this.username,
    is_admin: false, //always false through the registration endpoint
    token: this.generateJWT(),
  };
};

UsersSchema.methods.toJSON = function() {
  return {
    _id: this._id,
    email: this.email,
    birth_date: this.birth_date,
    first_name: this.first_name,
    surname: this.surname,
    username: this.username,
    is_admin: false, //always false through the registration endpoin
    achievements: this.achievements,
    metadata: this.metadata
  };
};

mongoose.model('Users', UsersSchema);