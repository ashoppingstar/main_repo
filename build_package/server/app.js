const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');
//see if can be trimmed
const session = require('express-session');
const cors = require('cors');
// endsee
const mongoose = require('mongoose');
const errorHandler = require('errorhandler');
var cookieParser = require('cookie-parser');


//HTTPS cert init

var fs = require('fs')
var https = require('https')


//Configure mongoose's promise to global promise
mongoose.promise = global.Promise;

//Configure isProduction variable
const isProduction = process.env.NODE_ENV === 'production';

//Initiate our app
const app = express();


//Configure our app
//app.use(cors());


app.use(require('morgan')('dev'));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(express.static(path.join(__dirname, 'public')));
app.use(session({ name:'session' , secret: 'ashoppingstar', cookie: { maxAge: 60000 }, resave: false, saveUninitialized: false }));
app.use(cookieParser());


if(!isProduction) {
  app.use(errorHandler());
}

//get arguments
var args = process.argv.slice(2);

//Configure Mongoose
mongoose.connect('mongodb://'+args[0]+':27017/softeng');
mongoose.set('debug', true);

//Models & routes

app.use(cors({credentials: true}));
require('./models/Users');
require('./models/Activities');
require('./models/Sportscenters');
require('./models/Blacklists');
require('./models/ActivitySportscenters');
require('./models/Prices');
require('./models/Metadatas');

require('./config/passport');
app.use(require('./routes'));

//error handling 

app.use(function (err, req, res, next) {
  if (err.name === 'UnauthorizedError') {
    res.status(403).send('You are not allowed to do such action');
  }
});

if(!isProduction) {
  app.use((req, res, err) => {
    res.status(err.status || 500);

    res.json({
      errors: {
        message: err.message,
        error: err,
      },
    });
  });
}

app.use((req, res,err) => {
  res.status(err.status || 500);

  res.json({
    errors: {
      message: err.message,
      error: {},
    },
  });
});


https.createServer({
  key: fs.readFileSync('server.key'),
  cert: fs.readFileSync('server.cert')
}, app)
.listen(8765, function () {
  console.log('Server running on https://localhost:8765/')
})


var http = require('http')
http.createServer(app).listen(8766, function() {
    console.log('Express HTTP server listening on port 8765');
});

//app.listen(8000, () => console.log('Server running on http://localhost:8000/'));
